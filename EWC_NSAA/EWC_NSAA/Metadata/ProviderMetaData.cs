﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace EWC_NSAA.Metadata
{
    public class ProviderMetaData
    {
        [Required(ErrorMessage = "PCP Name is required.")]
        [StringLength(100, ErrorMessage = "PCP Name cannot be more than 100 characters long.")]
        public string PCPName { get; set; }

        [Required(ErrorMessage = "PCP NPI is required.")]
        [StringLength(10, ErrorMessage = "PCP NPI cannot be more than 10 characters long.")]
        public string PCP_NPI { get; set; }


        [Required(ErrorMessage = "PCP Owner is required.")]
        [StringLength(2, ErrorMessage = "PCP Owner cannot be more than 2 characters long.")]
        public string PCP_Owner { get; set; }

        [Required(ErrorMessage = "PCP Location is required.")]
        [StringLength(3, ErrorMessage = "PCP Location cannot be more than 3 characters long.")]
        public string PCP_Location { get; set; }

        [Required(ErrorMessage = "PCP Address1 is required.")]
        [StringLength(30, ErrorMessage = "PCP Address line 1 cannot be more than 30 characters long.")]
        public string PCPAddress1 { get; set; }

        [StringLength(30, ErrorMessage = "PCP Address line 2 cannot be more than 30 characters long.")]
        public string PCPAddress2 { get; set; }

        [Required(ErrorMessage = "PCP City is required.")]
        [StringLength(24, ErrorMessage = "PCP City cannot be more than 24 characters long.")]
        public string PCPCity { get; set; }

        [Required(ErrorMessage = "PCP State is required.")]
        [StringLength(2, ErrorMessage = "PCP State cannot be more than 2 characters long.")]
        public string PCPState { get; set; }

        [Required(ErrorMessage = "PCP Zip is required.")]
        [StringLength(5, ErrorMessage = "PCP Zip cannot be more than 5 characters long.")]
        public string PCPZip { get; set; }

        [StringLength(50, ErrorMessage = "PCP Contact First Name cannot be more than 50 characters long.")]
        public string PCPContactFName { get; set; }

        [StringLength(50, ErrorMessage = "PCP Contact Last Name cannot be more than 50 characters long.")]
        public string PCPContactLName { get; set; }

        [StringLength(50, ErrorMessage = "PCP Contact Title cannot be more than 50 characters long.")]
        public string PCPContactTitle { get; set; }

        [StringLength(10, ErrorMessage = "PCP Contact Telephone cannot be more than 10 characters long.")]
        public string PCPContactTelephone { get; set; }

        [StringLength(45, ErrorMessage = "PCP Contact Email cannot be more than 45 characters long.")]
        public string PCPContactEmail { get; set; }

        [StringLength(100, ErrorMessage = "Medical Specialty cannot be more than 100 characters long.")]
        public string MedicalSpecialtyOther { get; set; }

        [StringLength(100, ErrorMessage = "Managed Care Plan cannot be more than 100 characters long.")]
        public string ManagedCarePlanOther { get; set; }
    }
}