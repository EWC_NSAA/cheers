﻿using System;
using System.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EWC_NSAA.Common
{
    public class ConfigHelper
    {
        public static string GetConfigData(string msgKey)
        {
            return (ConfigurationManager.AppSettings[msgKey] + "");
        }


        public static string GetConfigData(string msgKey, string defaultVal)
        {
            string data = ConfigurationManager.AppSettings[msgKey] + "";
            return ((data.Length > 0) ? data : defaultVal);
        }

        public static int GetConfigDataInt(string key, int defaultVal)
        {

            short result = 0;
            string sMax = ConfigurationManager.AppSettings[key] + "";
            if (sMax.Length > 0)
            {

                if (Int16.TryParse(sMax, out result))
                    return result;
            }

            return defaultVal;


        }

        public static bool KeyExists(string findKey)
        {
            // 1.0.0.3 


            findKey = findKey.ToLower();

            string[] keys = ConfigurationManager.AppSettings.AllKeys;

            if (keys != null)
            {
                for (int i = 0; i < keys.Length; ++i)
                {
                    if (keys[i].ToLower() == findKey)
                        return true;
                }
            }

            return false;
        }


    }
}