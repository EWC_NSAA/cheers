﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using EWC_NSAA;
using System.Reflection;
using System.Net;
using EWC_NSAA.EWC_NSAA_Service_XML;

namespace EWC_NSAA.Controllers
{
    public class BaseController : Controller
    {
        /*
        '  Web Services
        ' ----------------------------------------------------
        ' Minutes to wait for service to respond*/
        private const int MINUTES_TO_WAIT = 10;
        // convert each minute to millisecs, use this var to set the proxy timeout value
        private const int SERVICE_WAIT_TIME = 60000 * MINUTES_TO_WAIT;


        #region GetVersion
        /* -------------------------------------------------------------------------
		'   Function:       GetVersion
		'
		'   Description:    To get the version info from the assembly at run time
		'                   
		'   Parameters:     none
		'
		'   Return Value:	string containing the 4 part version as: 1.2.3.4

		'   Notes:          
		'
		'   Side Effects:	
		'
		'	Modifications:
		'		By						Date			Purpose
		'		-----------------		--------		-------------------------------
		'		robert calmann			11/23/03			created
		'
		' ------------------------------------------------------------------------- */
        public static string GetVersion()
        {

            //int major = Assembly.GetExecutingAssembly().GetName().Version.Major;
            //int minor = Assembly.GetExecutingAssembly().GetName().Version.Minor;
            //int rev = Assembly.GetExecutingAssembly().GetName().Version.Revision;
            //int build = Assembly.GetExecutingAssembly().GetName().Version.Build;
            string v = Assembly.GetExecutingAssembly().GetName().Version.ToString();
            return v;



        }
        #endregion

        #region GetAppName
        /* -------------------------------------------------------------------------
		'   Function:       GetAppName
		'
		'   Description:    To get the name of this application
		'                   
		'   Parameters:     fullName -	true=return the full name (App, version, culture, public key)
		'								false=return name only (App)
		'
		'   Return Value:	string as per fullname value

		'   Notes:          
		'
		'   Side Effects:	
		'
		'	Modifications:
		'		By						Date			Purpose
		'		-----------------		--------		-------------------------------
		'		robert calmann			02/01/05			created
		'
		' ------------------------------------------------------------------------- */

        public static string GetAppName(bool fullName)
        {
            string name = "";

            if (fullName)
                name = Assembly.GetExecutingAssembly().GetName().FullName;
            else
                name = Assembly.GetExecutingAssembly().GetName().Name;


            return name;
        }





        #endregion

        #region "WebService Proxy"

        public static EWC_NSAA_Service_XML.EWC_NSAA_WebService_XML getProxy()
        {
          
            EWC_NSAA_Service_XML.EWC_NSAA_WebService_XML proxy = new EWC_NSAA_Service_XML.EWC_NSAA_WebService_XML();

            proxy.Credentials = CredentialCache.DefaultCredentials;

            proxy.Timeout = SERVICE_WAIT_TIME;

            return proxy;

        }


        #endregion


    }
}