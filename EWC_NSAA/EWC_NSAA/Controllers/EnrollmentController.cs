﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using EWC_NSAA.Models;
using EWC_NSAA.RaceCBL.Models;
using EWC_NSAA.ViewModels;
using System.Collections;
using System.Data.Entity.Core.Objects;
using System.Data.Entity.Infrastructure;
using EWC_NSAA.Common;
using EWC_NSAA.CaregiverApprovalCBL.Models;

// For using NavigationCycle ViewModels functions
using EWC_NSAA.ContactPreferenceCBL.Models;

namespace EWC_NSAA.Controllers
{
    //[Authorize(Roles = @"EWC_NSAA_Contributors, EWC_NSAA_A")]
    public class EnrollmentController : BaseController
    {        
        // GET: Enrollment
        public ActionResult Index()
        {
            RecipientListViewModel RecipVM = new RecipientListViewModel();            
            List<trProvLanguages> lsLanguages = new List<trProvLanguages>();
            List<tServiceRecipient> lsRecipients = new List<tServiceRecipient>();

            try
            {
                lsLanguages = GetLanguagesFromDataSet();
                lsRecipients = GetRecipientsFromDataSet();

                RecipVM.Languages = lsLanguages;
                RecipVM.Recipients = lsRecipients;
            }
            catch (Exception e)
            {
                throw; //Errors.PublishException(e, GetAppName(false), GetVersion());
            }
            
            return View(RecipVM);
        }

        public ActionResult ViewEnrolledRecipient()
        {
            RecipientListViewModel RecipVM = new RecipientListViewModel();
            List<tServiceRecipient> lsRecipients = new List<tServiceRecipient>();

            try
            {
                lsRecipients = GetRecipientsFromDataSet();
                RecipVM.Recipients = lsRecipients;

            }
            catch (Exception e)
            {
                throw; //Errors.PublishException(e, GetAppName(false), GetVersion());
            }

            return View(RecipVM);
        }
        
        // GET: Enrollment/Create        
        public ActionResult Create()
        {
            tServiceRecipient tservicerecipient = new tServiceRecipient();
            var tservicerecipientVM = new RecipientViewModel();
                        
            //Race setup
            var selectedRace = new List<Race>();

            //Caregiver Approval setup
            var selectedCaregiverApproval = new List<CaregiverApprovals>();

            var tNSProvider = new tNSProvider();
            var selectedWeekday = new List<WeekdayContactPreference>();
            var selectedTime = new List<TimeContactPreference>();

            try
            {
                tservicerecipientVM.AvailableRace = GetAll().ToList();
                tservicerecipientVM.SelectedRace = selectedRace;

                tservicerecipientVM.AvailableApprovals = GetAllApprovals().ToList();
                tservicerecipientVM.SelectedApprovals = selectedCaregiverApproval;
               
                //Defaut State to CA
                tservicerecipient.State = "CA";

                tservicerecipientVM.tServiceRecipient = tservicerecipient;

                if (tservicerecipientVM.tServiceRecipient.DOB == DateTime.MinValue)
                {
                    tservicerecipientVM.tServiceRecipient.DOB = null;
                }

                tservicerecipientVM.AvailableWeekday = GetAllWeekdays().ToList();
                tservicerecipientVM.SelectedWeekday = selectedWeekday;

                //Time setup
                tservicerecipientVM.AvailableTime = GetAllTimes().ToList();
                tservicerecipientVM.SelectedTime = selectedTime;

                tNSProvider.NSProviderID = 0;
                tservicerecipientVM.tNSProvider = tNSProvider;


            }
            catch (Exception e)
            {
                throw; //Errors.PublishException(e, GetAppName(false), GetVersion());
            }

            return View(tservicerecipientVM);
        }

        // POST: Enrollment/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]       
        public ActionResult Create([Bind (Exclude="RaceID")] RecipientViewModel RecipVM)//[Bind(Include = "FK_RecipID,LastName,FirstName,MiddleInitial,DOB,AddressNotAvailable,Address1,Address2,City,State,Zip,Phone1,P1PhoneType,P1PhoneTypeOther,P1PersonalMessage,Phone2,P2PhoneType,P2PhoneTypeOther,P2PersonalMessage,MotherMaidenName,Email,SSN,ImmigrantStatus,CountryOfOrigin,Gender,Race1,Race1Other,Race2,Ethnicity,PrimaryLanguage,AgreeToSpeakEnglish,CaregiverName,Relationship,RelationshipOther,CaregiverPhone,CaregiverPhoneType,CaregiverPhoneTypeOther,CaregiverPhonePersonalMessage,CaregiverEmail,CaregiverContactPreference,Comments")] RecipientViewModel Test)
        {
              
            int NSID = 0;
            int NavigatorID = 0;

            if (ModelState.IsValid)
            {
                //Race 
                var RaceList = GetRaceList(RecipVM);
                //Caregiver Approval
                var ApprovalList = GetCaregiverApprovalList(RecipVM);

                RecipVM.tServiceRecipient.CreatedBy = "";

                try
                {
                    RecipVM.tServiceRecipient.FK_NavigatorID = GetNavigatorID(); //NavigatorVM.tNavigator.NavigatorID;
                    NavigatorID = GetNavigatorID();
                }
                catch (Exception e)
                {
                    throw; //Errors.PublishException(e, GetAppName(false), GetVersion());
                }
                

                try
                {
                        int retValue = getProxy().InsertServiceRecipient(RecipVM.tServiceRecipient.FK_NavigatorID, 
                        RecipVM.tServiceRecipient.FK_RecipID, 
                        RecipVM.tServiceRecipient.LastName, 
                        RecipVM.tServiceRecipient.FirstName, 
                        RecipVM.tServiceRecipient.MiddleInitial,
                        RecipVM.tServiceRecipient.DOB,
                        RecipVM.tServiceRecipient.AddressNotAvailable, 
                        RecipVM.tServiceRecipient.Address1, 
                        RecipVM.tServiceRecipient.Address2, 
                        RecipVM.tServiceRecipient.City, 
                        RecipVM.tServiceRecipient.State, 
                        RecipVM.tServiceRecipient.Zip,
                        RecipVM.tServiceRecipient.Phone1, 
                        RecipVM.tServiceRecipient.P1PhoneType, 
                        RecipVM.tServiceRecipient.P1PersonalMessage, 
                        RecipVM.tServiceRecipient.Phone2, 
                        RecipVM.tServiceRecipient.P2PhoneType, 
                        RecipVM.tServiceRecipient.P2PersonalMessage,
                        RecipVM.tServiceRecipient.MotherMaidenName, 
                        RecipVM.tServiceRecipient.Email, 
                        RecipVM.tServiceRecipient.SSN, 
                        RecipVM.tServiceRecipient.ImmigrantStatus, 
                        RecipVM.tServiceRecipient.CountryOfOrigin, 
                        RecipVM.tServiceRecipient.Gender,
                        RecipVM.tServiceRecipient.Ethnicity,
                        RecipVM.tServiceRecipient.PrimaryLanguage, 
                        RecipVM.tServiceRecipient.AgreeToSpeakEnglish, 
                        RecipVM.tServiceRecipient.CaregiverName, 
                        RecipVM.tServiceRecipient.Relationship, 
                        RecipVM.tServiceRecipient.RelationshipOther,
                        RecipVM.tServiceRecipient.CaregiverPhone, 
                        RecipVM.tServiceRecipient.CaregiverPhoneType, 
                        RecipVM.tServiceRecipient.CaregiverPhonePersonalMessage, 
                        RecipVM.tServiceRecipient.CaregiverEmail, 
                        RecipVM.tServiceRecipient.CaregiverContactPreference, 
                        RecipVM.tServiceRecipient.Comments);

                    NSID = retValue;

                    if(RecipVM.tRace.RaceOther == null)
                    {
                        RecipVM.tRace.RaceOther = "";
                    }
                    getProxy().SaveRace(NSID, RaceList, RecipVM.tRace.RaceOther);

                    if(RecipVM.tCaregiverApprovals == null)
                    {
                        tCaregiverApprovals tCaregiverApprovals = new tCaregiverApprovals();
                        RecipVM.tCaregiverApprovals = tCaregiverApprovals;
                    }

                    if (RecipVM.tCaregiverApprovals.CaregiverApprovalOther == null)
                    {
                        RecipVM.tCaregiverApprovals.CaregiverApprovalOther = "";
                    }
                    getProxy().SaveCaregiverApproval(NSID, ApprovalList, RecipVM.tCaregiverApprovals.CaregiverApprovalOther);

                    // Calling a new Local Function
                    Save(RecipVM, NSID, NavigatorID);

                    TempData["FeedbackMessage"] = "Your changes were saved successfully.";
                }
                catch (System.Data.Entity.Validation.DbEntityValidationException e)
                {
                    foreach (var eve in e.EntityValidationErrors)
                    {
                        Console.WriteLine("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                            eve.Entry.Entity.GetType().Name, eve.Entry.State);
                        foreach (var ve in eve.ValidationErrors)
                        {
                            Console.WriteLine("- Property: \"{0}\", Error: \"{1}\"",
                                ve.PropertyName, ve.ErrorMessage);
                        }
                    }
                    throw; //Errors.PublishException(e, GetAppName(false), GetVersion());
                }

                return RedirectToAction("ViewEnrolledRecipient", "Enrollment");
                //return View(RecipVM);
               
            }
            else
            {
                var allErrors = ModelState.Values.SelectMany(v => v.Errors);
                tServiceRecipient tservicerecipient = new tServiceRecipient();
                var tservicerecipientVM = new RecipientViewModel();

                //Race setup
                var selectedRace = new List<Race>();
                //Caregiver Approval setup
                var selectedCaregiverApproval = new List<CaregiverApprovals>();

                try
                {
                    tservicerecipientVM.AvailableRace = GetAll().ToList();
                    tservicerecipientVM.SelectedRace = selectedRace;

                    tservicerecipientVM.AvailableApprovals = GetAllApprovals().ToList();
                    tservicerecipientVM.SelectedApprovals = selectedCaregiverApproval;

                    tservicerecipientVM.tServiceRecipient = tservicerecipient;

                }
                catch (Exception e)
                {
                    throw; //Errors.PublishException(e, GetAppName(false), GetVersion());
                }
                
                return View(tservicerecipientVM);
            }

            
        }
        
        // GET: Enrollment/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tServiceRecipient tServiceRecipient = GetRecipientFromDataSet((int)id);
            if (tServiceRecipient == null)
            {
                return HttpNotFound();
            }
            RecipientViewModel recipVM = new RecipientViewModel();
            tRace tRace = new tRace();
            tCaregiverApprovals tCaregiverApprovals = new tCaregiverApprovals();

            List<Race> ls = new List<Race>();
            ls = GetRaceFromDataSet((int)id);

            recipVM.RaceAsianCode = GetAsianRaceFromDataSet((int)id);

            recipVM.RacePacIslanderCode = GetPacIslanderRaceFromDataSet((int)id);

            tRace.RaceOther = GetRaceOtherFromDataSet((int)id);

            recipVM.SelectedRace = ls;
            recipVM.AvailableRace = GetAll().ToList();

            List<CaregiverApprovals> ls1 = new List<CaregiverApprovals>();
            ls1 = GetApprovalFromDataSet((int)id);

            tCaregiverApprovals.CaregiverApprovalOther = GetApprovalOtherFromDataSet((int)id);

            recipVM.SelectedApprovals = ls1;
            recipVM.AvailableApprovals = GetAllApprovals().ToList();

            recipVM.tServiceRecipient = tServiceRecipient;
            recipVM.tRace = tRace;
            recipVM.tCaregiverApprovals = tCaregiverApprovals;
            return View(recipVM);
        }

        public ActionResult NavigatorTransfer(int NSID, int NavigatorID)
        {
            NavigatorTransferViewModel navigatortransferVM = new NavigatorTransferViewModel();

            DataSet dsNavigatorTransfer = getProxy().GetNavigatorInfo(1, NSID, NavigatorID, 0, DateTime.MinValue, 0);

            DataSet dsRecipient = getProxy().GetNavigatorInfo(4, NSID, NavigatorID, 0, DateTime.MinValue, 0);

            navigatortransferVM.NSRecipientID = NSID;
            navigatortransferVM.OldNavigatorID = NavigatorID;
            navigatortransferVM.FirstName = dsNavigatorTransfer.Tables[0].Rows[0]["FirstName"].ToString();
            navigatortransferVM.LastName = dsNavigatorTransfer.Tables[0].Rows[0]["LastName"].ToString();
            navigatortransferVM.Region = Convert.IsDBNull(dsNavigatorTransfer.Tables[0].Rows[0]["Region"]) ? 0 : Convert.ToInt32(dsNavigatorTransfer.Tables[0].Rows[0]["Region"]);

            navigatortransferVM.RecipFirstName = dsRecipient.Tables[0].Rows[0]["FirstName"].ToString();
            navigatortransferVM.RecipLastName = dsRecipient.Tables[0].Rows[0]["LastName"].ToString();
            navigatortransferVM.Language = dsRecipient.Tables[0].Rows[0]["PrimaryLanguage"].ToString();
            navigatortransferVM.AgreeToSpeakEnglish = dsRecipient.Tables[0].Rows[0]["AgreeToSpeakEnglish"].ToString();

            navigatortransferVM.NavigatorTransferDt = DateTime.Today;

            int NavigatorType = Convert.IsDBNull(dsNavigatorTransfer.Tables[0].Rows[0]["Type"]) ? 0 : Convert.ToInt32(dsNavigatorTransfer.Tables[0].Rows[0]["Type"]);

            switch (NavigatorType)
            {
                case 0:
                    navigatortransferVM.Type = "";
                    break;
                case 1:
                    navigatortransferVM.Type = "Health Educator";
                    break;
                case 2:
                    navigatortransferVM.Type = "Clinical Coordinator";
                    break;
                case 3:
                    navigatortransferVM.Type = "CHW";
                    break;
                case 4:
                    navigatortransferVM.Type = "Other";
                    break;
            }

                    return View(navigatortransferVM);
        }
  

        #region GetPhoneTypeDropDown

        public static List<SelectListItem> GetPhoneTypeDropDown()
        {
            List<SelectListItem> ls = new List<SelectListItem>();

            ls.Add(new SelectListItem() { Text = "Home", Value = "1" });
            ls.Add(new SelectListItem() { Text = "Cell/Mobile", Value = "2" });
            ls.Add(new SelectListItem() { Text = "Message", Value = "3" });
            ls.Add(new SelectListItem() { Text = "Work", Value = "4" });

            return ls;
        }

        #endregion

        #region GetCaregiverContactPrefDropDown
        public static List<SelectListItem> GetCaregiverContactPrefDropDown()
        {
            List<SelectListItem> ls = new List<SelectListItem>();

            //ls.Add(new SelectListItem() { Text = "", Value = "0" });
            ls.Add(new SelectListItem() { Text = "Phone Call", Value = "1" });
            ls.Add(new SelectListItem() { Text = "E-mail", Value = "2" });
            ls.Add(new SelectListItem() { Text = "Text", Value = "3" });
            ls.Add(new SelectListItem() { Text = "Face to Face", Value = "4" });

            return ls;
        }

        #endregion

        #region GetCaregiverRelationshipDropDown
        public static List<SelectListItem> GetCaregiverRelationshipDropDown()
        {
            List<SelectListItem> ls = new List<SelectListItem>();

            ls.Add(new SelectListItem() { Text = "Family Support", Value = "1" });
            ls.Add(new SelectListItem() { Text = "Community Support", Value = "2" });
            ls.Add(new SelectListItem() { Text = "Professional Caretaker", Value = "3" });
            ls.Add(new SelectListItem() { Text = "Shelter", Value = "4" });
            ls.Add(new SelectListItem() { Text = "Other", Value = "5" });

            return ls;
        }

        #endregion

        #region GetCountriesDropDown
        public static List<SelectListItem> GetCountriesDropDown()
        {
            List<SelectListItem> ls = new List<SelectListItem>();
            
            //get values from web service

            DataSet countriesDS = getProxy().GetCountriesList();

            for(var index = 0; index < countriesDS.Tables[0].Rows.Count; index++)
            {
                ls.Add(new SelectListItem() { Text = countriesDS.Tables[0].Rows[index]["CountryName"].ToString(), Value = countriesDS.Tables[0].Rows[index]["CountryCode"].ToString() });
            }


            return ls;
        }

        #endregion

        #region GetLanguagesDropDown
        public static List<SelectListItem> GetLanguagesDropDown()
        {
            List<SelectListItem> ls = new List<SelectListItem>();

            DataSet languagesDS = getProxy().GetLanguagesList();

            for (var index = 0; index < languagesDS.Tables[0].Rows.Count; index++)
            {
                ls.Add(new SelectListItem() { Text = languagesDS.Tables[0].Rows[index]["LanguageName"].ToString(), Value = languagesDS.Tables[0].Rows[index]["LanguageCode"].ToString() });
            }


            return ls;
        }
        #endregion

        #region GetNavigatorID
        public int GetNavigatorID()
        {
            int NavigatorID = 0;
            string user = System.Security.Principal.WindowsIdentity.GetCurrent().Name;

            DataSet NavigatorDS;

            try
            {
                //NavigatorDS = getProxy().GetNavigator(User.Identity.Name);
                NavigatorDS = getProxy().GetNavigator(user);
                NavigatorID = (int)NavigatorDS.Tables[0].Rows[0]["NavigatorID"];
              

            }
            catch (Exception e)
            {
                throw; //Errors.PublishException(e, GetAppName(false), GetVersion());
            }

          return NavigatorID;

        }
        #endregion

        #region GetNavigatorsDropDown
        public static List<SelectListItem> GetNavigatorsDropDown()
        {
            List<SelectListItem> ls = new List<SelectListItem>();

            //get values from web service

            DataSet navigatorsDS = getProxy().GetNavigatorsList(3);

            for (var index = 0; index < navigatorsDS.Tables[0].Rows.Count; index++)
            {
                ls.Add(new SelectListItem() { Text = navigatorsDS.Tables[0].Rows[index]["FirstName"].ToString() + " " + navigatorsDS.Tables[0].Rows[index]["LastName"].ToString(), Value = navigatorsDS.Tables[0].Rows[index]["NavigatorID"].ToString() });
            }


            return ls;
        }

        #endregion

        #region GetNavigatorTRDropDown
        public static List<SelectListItem> GetNavigatorTRDropDown()
        {
            List<SelectListItem> ls = new List<SelectListItem>();

            ls.Add(new SelectListItem() { Text = "Reason 1", Value = "1" });
            ls.Add(new SelectListItem() { Text = "Reason 2", Value = "2" });
            ls.Add(new SelectListItem() { Text = "Reason 3", Value = "3" });
            ls.Add(new SelectListItem() { Text = "Reason 4", Value = "4" });

            return ls;
        }

        #endregion

        #region Race
        /// <summary>
        /// for get all Race
        /// </summary>
        public static IEnumerable<Race> GetAll()
        {
            List<Race> ls = new List<Race>();

            DataSet raceDS = getProxy().GetRaceList();

            for (var index = 0; index < raceDS.Tables[0].Rows.Count; index++)
            {
                ls.Add(new Race() { RaceName = raceDS.Tables[0].Rows[index]["RaceName"].ToString(), RaceCode = (int)raceDS.Tables[0].Rows[index]["RaceCode"] });
            }

            return ls;
        }

        /// <summary>
        /// for setup view model, after post the user selected race data
        /// </summary>
        private RecipientViewModel GetRaceModel(PostedRace postedRace)
        {
            // setup properties
            var model = new RecipientViewModel();
            var selectedRace = new List<Race>();
            var postedRaceIds = new string[0];
            if (postedRace == null) postedRace = new PostedRace();

            // if a view model array of posted race ids exists
            // and is not empty,save selected ids
            if (postedRace.RaceIds != null && postedRace.RaceIds.Any())
            {
                postedRaceIds = postedRace.RaceIds;
            }

            // if there are any selected ids saved, create a list of race
            if (postedRaceIds.Any())
            {
                selectedRace = GetAll()
                 .Where(x => postedRaceIds.Any(s => x.RaceCode.ToString().Equals(s)))
                 .ToList();
            }

            //setup a view model
            model.AvailableRace = GetAll().ToList();
            model.SelectedRace = selectedRace;
            model.postedRace = postedRace;
            return model;
        }


        private string GetRaceList(RecipientViewModel RecipVM)
        {
            var selectedRace = new List<Race>();
            var postedRaceIds = new string[0];
            var postedRace = new PostedRace();
            postedRace = RecipVM.postedRace;
            // if a view model array of posted race ids exists
            // and is not empty,save selected ids
            if (postedRace != null && postedRace.RaceIds != null && postedRace.RaceIds.Any())
            {
                postedRaceIds = postedRace.RaceIds;
            }

            // if there are any selected ids saved, create a list of race
            if (postedRaceIds.Any())
            {
                selectedRace = GetAll()
                 .Where(x => postedRaceIds.Any(s => x.RaceCode.ToString().Equals(s)))
                 .ToList();
            }

            //setup a view model
            RecipVM.AvailableRace = GetAll().ToList();
            RecipVM.SelectedRace = selectedRace;
            RecipVM.postedRace = postedRace;


            string RaceList = "";
            if(postedRace != null)
            {
                foreach (string RaceId in postedRace.RaceIds)
                {
                    if (RaceId != null)
                    {
                        RaceList = RaceList + RaceId + ",";
                    }

                }
            }
            
            if (RecipVM.RaceAsianCode > 0)
            {
                //Add any Asian Race code
                RaceList = RaceList + RecipVM.RaceAsianCode.ToString() + ",";
            }

            if (RecipVM.RacePacIslanderCode > 0)
            {
                //Add any Pac Islander Race code
                RaceList = RaceList + RecipVM.RacePacIslanderCode.ToString() + ",";
            }

            if(RaceList.Length > 0)
            {
                RaceList = RaceList.Substring(0, RaceList.Length - 1);
            }
            
            //End race
            return RaceList;

        }

        #endregion

        #region GetRaceBlackAfricanDropDown
        public static List<SelectListItem> GetRaceBlackAfricanDropDown()
        {
            List<SelectListItem> ls = new List<SelectListItem>();

            ls.Add(new SelectListItem() { Text = "Dummy_A", Value = "1" });
            ls.Add(new SelectListItem() { Text = "Dummy_B", Value = "2" });
            ls.Add(new SelectListItem() { Text = "Dummy_C", Value = "3" });
            ls.Add(new SelectListItem() { Text = "Dummy_D", Value = "4" });

            return ls;
        }
        #endregion

        #region GetRaceAsianDropDown
        public static List<SelectListItem> GetRaceAsianDropDown()
        {
            List<SelectListItem> ls = new List<SelectListItem>();

            //Add 0 Option
            ls.Add(new SelectListItem() { Text = "--Select--", Value = "0" });

            DataSet raceAsianDS = getProxy().GetRaceAsianList();

            for (var index = 0; index < raceAsianDS.Tables[0].Rows.Count; index++)
            {
                ls.Add(new SelectListItem() { Text = raceAsianDS.Tables[0].Rows[index]["RaceAsianName"].ToString(), Value = raceAsianDS.Tables[0].Rows[index]["RaceAsianCode"].ToString() });
            }

            return ls;
        }

        #endregion

        #region GetRacePacIslanderDropDown
        public static List<SelectListItem> GetRacePacIslanderDropDown()
        {
            List<SelectListItem> ls = new List<SelectListItem>();

            //Add 0 Option
            ls.Add(new SelectListItem() { Text = "--Select--", Value = "0" });

            DataSet racePacIslanderDS = getProxy().GetRacePacIslanderList();

            for (var index = 0; index < racePacIslanderDS.Tables[0].Rows.Count; index++)
            {
                ls.Add(new SelectListItem() { Text = racePacIslanderDS.Tables[0].Rows[index]["RacePacIslanderName"].ToString(), Value = racePacIslanderDS.Tables[0].Rows[index]["RacePacIslanderCode"].ToString() });
            }

            return ls;
        }

        #endregion

        #region Get Languages from DataSet
        public List<trProvLanguages> GetLanguagesFromDataSet()
        {
            List<trProvLanguages> lsLanguages = new List<trProvLanguages>();

            try
            {
                DataSet languagesDS = getProxy().GetLanguagesList();
                for (var index = 0; index < languagesDS.Tables[0].Rows.Count; index++)
                {
                    lsLanguages.Add(new trProvLanguages() { LanguageName = languagesDS.Tables[0].Rows[index]["LanguageName"].ToString(), LanguageCode = (int)languagesDS.Tables[0].Rows[index]["LanguageCode"] });
                }

            }
            catch
            {
                throw;
            }
            
            return lsLanguages;
        }

        #endregion

        #region Get Recipients From DataSet
        private List<tServiceRecipient> GetRecipientsFromDataSet()
        {
            List<tServiceRecipient> lsRecipients = new List<tServiceRecipient>();
            try
            {
                DataSet recipientsDS = getProxy().SelectServiceRecipient(GetNavigatorID());
                for (var index = 0; index < recipientsDS.Tables[0].Rows.Count; index++)
                {

                    lsRecipients.Add(new tServiceRecipient()
                    {
                        NSRecipientID = (int)recipientsDS.Tables[0].Rows[index]["NSRecipientID"],
                        FK_NavigatorID = (int)recipientsDS.Tables[0].Rows[index]["FK_NavigatorID"],
                        FK_RecipID = recipientsDS.Tables[0].Rows[index]["FK_RecipID"].ToString(),
                        LastName = recipientsDS.Tables[0].Rows[index]["LastName"].ToString(),
                        FirstName = recipientsDS.Tables[0].Rows[index]["FirstName"].ToString(),
                        MiddleInitial = recipientsDS.Tables[0].Rows[index]["MiddleInitial"].ToString(),
                        DOB = (DateTime)recipientsDS.Tables[0].Rows[index]["DOB"],
                        Address1 = recipientsDS.Tables[0].Rows[index]["Address1"].ToString(),
                        Address2 = recipientsDS.Tables[0].Rows[index]["Address2"].ToString(),
                        City = recipientsDS.Tables[0].Rows[index]["City"].ToString(),
                        State = recipientsDS.Tables[0].Rows[index]["State"].ToString(),
                        Zip = recipientsDS.Tables[0].Rows[index]["Zip"].ToString(),
                        Phone1 = recipientsDS.Tables[0].Rows[index]["Phone1"].ToString(),
                        P1PhoneType = (short)recipientsDS.Tables[0].Rows[index]["P1PhoneType"],
                        P1PersonalMessage = Convert.IsDBNull(recipientsDS.Tables[0].Rows[index]["P1PersonalMessage"]) ? false : (bool)recipientsDS.Tables[0].Rows[index]["P1PersonalMessage"],
                        Phone2 = recipientsDS.Tables[0].Rows[index]["Phone2"].ToString(),
                        P2PhoneType = Convert.IsDBNull(recipientsDS.Tables[0].Rows[index]["P2PhoneType"]) ? 0 : (short?)recipientsDS.Tables[0].Rows[index]["P2PhoneType"],
                        P2PersonalMessage = Convert.IsDBNull(recipientsDS.Tables[0].Rows[index]["P2PersonalMessage"]) ? false : (bool)recipientsDS.Tables[0].Rows[index]["P2PersonalMessage"],
                        MotherMaidenName = recipientsDS.Tables[0].Rows[index]["MotherMaidenName"].ToString(),
                        Email = recipientsDS.Tables[0].Rows[index]["Email"].ToString(),
                        SSN = recipientsDS.Tables[0].Rows[index]["SSN"].ToString(),
                        ImmigrantStatus = Convert.IsDBNull(recipientsDS.Tables[0].Rows[index]["ImmigrantStatus"]) ? false : (bool)recipientsDS.Tables[0].Rows[index]["ImmigrantStatus"],
                        CountryOfOrigin = Convert.IsDBNull(recipientsDS.Tables[0].Rows[index]["CountryOfOrigin"]) ? 0 : (short?)recipientsDS.Tables[0].Rows[index]["CountryOfOrigin"],
                        Gender = recipientsDS.Tables[0].Rows[index]["Gender"].ToString(),
                        Ethnicity = Convert.IsDBNull(recipientsDS.Tables[0].Rows[index]["Ethnicity"]) ? false : (bool)recipientsDS.Tables[0].Rows[index]["Ethnicity"],
                        PrimaryLanguage = (short)recipientsDS.Tables[0].Rows[index]["PrimaryLanguage"],
                        AgreeToSpeakEnglish = Convert.IsDBNull(recipientsDS.Tables[0].Rows[index]["AgreeToSpeakEnglish"]) ? false : (bool)recipientsDS.Tables[0].Rows[index]["AgreeToSpeakEnglish"],
                        CaregiverName = recipientsDS.Tables[0].Rows[index]["CaregiverName"].ToString(),
                        Relationship = Convert.IsDBNull(recipientsDS.Tables[0].Rows[index]["Relationship"]) ? 0 : (short?)recipientsDS.Tables[0].Rows[index]["Relationship"],
                        RelationshipOther = recipientsDS.Tables[0].Rows[index]["RelationshipOther"].ToString(),
                        CaregiverPhone = recipientsDS.Tables[0].Rows[index]["CaregiverPhone"].ToString(),
                        CaregiverPhoneType = Convert.IsDBNull(recipientsDS.Tables[0].Rows[index]["CaregiverPhoneType"]) ? 0 : (short?)recipientsDS.Tables[0].Rows[index]["CaregiverPhoneType"],
                        CaregiverPhonePersonalMessage = Convert.IsDBNull(recipientsDS.Tables[0].Rows[index]["CaregiverPhonePersonalMessage"]) ? false : (bool)recipientsDS.Tables[0].Rows[index]["CaregiverPhonePersonalMessage"],
                        CaregiverEmail = recipientsDS.Tables[0].Rows[index]["CaregiverEmail"].ToString(),
                        CaregiverContactPreference = Convert.IsDBNull(recipientsDS.Tables[0].Rows[index]["CaregiverContactPreference"]) ? 0 : (int)recipientsDS.Tables[0].Rows[index]["CaregiverContactPreference"],
                        Comments = recipientsDS.Tables[0].Rows[index]["Comments"].ToString(),
                        xtag = recipientsDS.Tables[0].Rows[index]["xtag"].ToString(),
                        DateCreated = Convert.IsDBNull(recipientsDS.Tables[0].Rows[index]["DateCreated"]) ? DateTime.MinValue : (DateTime)recipientsDS.Tables[0].Rows[index]["DateCreated"],
                        CreatedBy = recipientsDS.Tables[0].Rows[index]["CreatedBy"].ToString(),
                        LastUpdated = Convert.IsDBNull(recipientsDS.Tables[0].Rows[index]["LastUpdated"]) ? DateTime.MinValue : (DateTime)recipientsDS.Tables[0].Rows[index]["LastUpdated"],
                        UpdatedBy = recipientsDS.Tables[0].Rows[index]["UpdatedBy"].ToString()
                    });
                }
                
            }
            catch
            {
                throw;
            }

            return lsRecipients;
        }

        #endregion

        #region Get Recipient From DataSet
        private tServiceRecipient GetRecipientFromDataSet(int NSID)
        {
            int index = 0;
            tServiceRecipient Recipient = new tServiceRecipient();
            try
            {
                DataSet recipientsDS = getProxy().GetServiceRecipient(NSID);
                Recipient = new tServiceRecipient()
                    {
                        NSRecipientID = (int)recipientsDS.Tables[0].Rows[index]["NSRecipientID"],
                        FK_NavigatorID = (int)recipientsDS.Tables[0].Rows[index]["FK_NavigatorID"],
                        FK_RecipID = recipientsDS.Tables[0].Rows[index]["FK_RecipID"].ToString(),
                        LastName = recipientsDS.Tables[0].Rows[index]["LastName"].ToString(),
                        FirstName = recipientsDS.Tables[0].Rows[index]["FirstName"].ToString(),
                        MiddleInitial = recipientsDS.Tables[0].Rows[index]["MiddleInitial"].ToString(),
                        DOB = (DateTime)recipientsDS.Tables[0].Rows[index]["DOB"],
                        Address1 = recipientsDS.Tables[0].Rows[index]["Address1"].ToString(),
                        Address2 = recipientsDS.Tables[0].Rows[index]["Address2"].ToString(),
                        City = recipientsDS.Tables[0].Rows[index]["City"].ToString(),
                        State = recipientsDS.Tables[0].Rows[index]["State"].ToString(),
                        Zip = recipientsDS.Tables[0].Rows[index]["Zip"].ToString(),
                        Phone1 = recipientsDS.Tables[0].Rows[index]["Phone1"].ToString(),
                        P1PhoneType = (short)recipientsDS.Tables[0].Rows[index]["P1PhoneType"],
                        P1PersonalMessage = Convert.IsDBNull(recipientsDS.Tables[0].Rows[index]["P1PersonalMessage"]) ? false : (bool)recipientsDS.Tables[0].Rows[index]["P1PersonalMessage"],
                        Phone2 = recipientsDS.Tables[0].Rows[index]["Phone2"].ToString(),
                        P2PhoneType = Convert.IsDBNull(recipientsDS.Tables[0].Rows[index]["P2PhoneType"]) ? 0 : (short?)recipientsDS.Tables[0].Rows[index]["P2PhoneType"],
                        P2PersonalMessage = Convert.IsDBNull(recipientsDS.Tables[0].Rows[index]["P2PersonalMessage"]) ? false : (bool)recipientsDS.Tables[0].Rows[index]["P2PersonalMessage"],
                        MotherMaidenName = recipientsDS.Tables[0].Rows[index]["MotherMaidenName"].ToString(),
                        Email = recipientsDS.Tables[0].Rows[index]["Email"].ToString(),
                        SSN = recipientsDS.Tables[0].Rows[index]["SSN"].ToString(),
                        ImmigrantStatus = Convert.IsDBNull(recipientsDS.Tables[0].Rows[index]["ImmigrantStatus"]) ? false : (bool)recipientsDS.Tables[0].Rows[index]["ImmigrantStatus"],
                        CountryOfOrigin = Convert.IsDBNull(recipientsDS.Tables[0].Rows[index]["CountryOfOrigin"]) ? 0 : (short?)recipientsDS.Tables[0].Rows[index]["CountryOfOrigin"],
                        Gender = recipientsDS.Tables[0].Rows[index]["Gender"].ToString(),
                        Ethnicity = Convert.IsDBNull(recipientsDS.Tables[0].Rows[index]["Ethnicity"]) ? false : (bool)recipientsDS.Tables[0].Rows[index]["Ethnicity"],
                        PrimaryLanguage = (short)recipientsDS.Tables[0].Rows[index]["PrimaryLanguage"],
                        AgreeToSpeakEnglish = Convert.IsDBNull(recipientsDS.Tables[0].Rows[index]["AgreeToSpeakEnglish"]) ? false : (bool)recipientsDS.Tables[0].Rows[index]["AgreeToSpeakEnglish"],
                        CaregiverName = recipientsDS.Tables[0].Rows[index]["CaregiverName"].ToString(),
                        Relationship = Convert.IsDBNull(recipientsDS.Tables[0].Rows[index]["Relationship"]) ? 0 : (short?)recipientsDS.Tables[0].Rows[index]["Relationship"],
                        RelationshipOther = recipientsDS.Tables[0].Rows[index]["RelationshipOther"].ToString(),
                        CaregiverPhone = recipientsDS.Tables[0].Rows[index]["CaregiverPhone"].ToString(),
                        CaregiverPhoneType = Convert.IsDBNull(recipientsDS.Tables[0].Rows[index]["CaregiverPhoneType"]) ? 0 : (short?)recipientsDS.Tables[0].Rows[index]["CaregiverPhoneType"],
                        CaregiverPhonePersonalMessage = Convert.IsDBNull(recipientsDS.Tables[0].Rows[index]["CaregiverPhonePersonalMessage"]) ? false : (bool)recipientsDS.Tables[0].Rows[index]["CaregiverPhonePersonalMessage"],
                        CaregiverEmail = recipientsDS.Tables[0].Rows[index]["CaregiverEmail"].ToString(),
                        CaregiverContactPreference = Convert.IsDBNull(recipientsDS.Tables[0].Rows[index]["CaregiverContactPreference"]) ? 0 : (int)recipientsDS.Tables[0].Rows[index]["CaregiverContactPreference"],
                        Comments = recipientsDS.Tables[0].Rows[index]["Comments"].ToString(),
                        xtag = recipientsDS.Tables[0].Rows[index]["xtag"].ToString(),
                        DateCreated = Convert.IsDBNull(recipientsDS.Tables[0].Rows[index]["DateCreated"]) ? DateTime.MinValue : (DateTime)recipientsDS.Tables[0].Rows[index]["DateCreated"],
                        CreatedBy = recipientsDS.Tables[0].Rows[index]["CreatedBy"].ToString(),
                        LastUpdated = Convert.IsDBNull(recipientsDS.Tables[0].Rows[index]["LastUpdated"]) ? DateTime.MinValue : (DateTime)recipientsDS.Tables[0].Rows[index]["LastUpdated"],
                        UpdatedBy = recipientsDS.Tables[0].Rows[index]["UpdatedBy"].ToString()
                    };

            }
            catch
            {
                throw;
            }

            return Recipient;
        }

        #endregion

        #region Get Race from DataSet
        private List<Race> GetRaceFromDataSet(int NSID)
        {
            List<Race> lsRace = new List<Race>();

            try
            {
                DataSet RaceDS = getProxy().GetRace(NSID);
                for (var index = 0; index < RaceDS.Tables[0].Rows.Count; index++)
                {
                    if ((int)RaceDS.Tables[0].Rows[index]["RaceCode"] < 10)
                    {
                        lsRace.Add(new Race() { RaceCode = (int)RaceDS.Tables[0].Rows[index]["RaceCode"], RaceName = RaceDS.Tables[0].Rows[index]["RaceName"].ToString(), IsSelected = true });
                    }
                }

            }
            catch
            {
                throw;
            }

            return lsRace;
        }

        #endregion

        #region Get Asian Race from DataSet
        private int GetAsianRaceFromDataSet(int NSID)
        {
            int raceAsian = 0;

            try
            {
                DataSet RaceAsianDS = getProxy().GetRace(NSID);
                for (var index = 0; index < RaceAsianDS.Tables[0].Rows.Count; index++)
                {
                    if ((int)RaceAsianDS.Tables[0].Rows[index]["RaceCode"] > 10 && (int)RaceAsianDS.Tables[0].Rows[index]["RaceCode"] < 21)
                    {
                        raceAsian = (int)RaceAsianDS.Tables[0].Rows[index]["RaceCode"];
                    }
                }

            }
            catch
            {
                throw;
            }

            return raceAsian;
        }

        #endregion

        #region Get Pac Islander Race from DataSet
        private int GetPacIslanderRaceFromDataSet(int NSID)
        {
            int racePacIslander = 0;

            try
            {
                DataSet RacePacIslanderDS = getProxy().GetRace(NSID);
                for (var index = 0; index < RacePacIslanderDS.Tables[0].Rows.Count; index++)
                {
                    if ((int)RacePacIslanderDS.Tables[0].Rows[index]["RaceCode"] > 10 && (int)RacePacIslanderDS.Tables[0].Rows[index]["RaceCode"] < 21)
                    {
                        racePacIslander = (int)RacePacIslanderDS.Tables[0].Rows[index]["RaceCode"];
                    }
                }

            }
            catch
            {
                throw;
            }

            return racePacIslander;
        }

        #endregion

        #region Get Race Other from DataSet
        private string GetRaceOtherFromDataSet(int NSID)
        {
            string raceOther = "";

            try
            {
                DataSet RaceAsianDS = getProxy().GetRace(NSID);
                
                if(RaceAsianDS.Tables[0].Rows.Count > 0)
                {
                    raceOther = RaceAsianDS.Tables[0].Rows[0]["RaceOther"].ToString();
                }
                


            }
            catch
            {
                throw;
            }

            return raceOther;
        }

        #endregion

        #region Caregiver Approval
        /// <summary>
        /// for get all Race
        /// </summary>
        public static IEnumerable<CaregiverApprovals> GetAllApprovals()
        {
            List<CaregiverApprovals> ls = new List<CaregiverApprovals>();

            DataSet approvalDS = getProxy().GetCaregiverApprovalList();

            for (var index = 0; index < approvalDS.Tables[0].Rows.Count; index++)
            {
                ls.Add(new CaregiverApprovals() { ApprovalDescription = approvalDS.Tables[0].Rows[index]["CaregiverApprovalDescription"].ToString(), ApprovalCode = (int)approvalDS.Tables[0].Rows[index]["CaregiverApprovalCode"] });
            }

            return ls;
        }

        /// <summary>
        /// for setup view model, after post the user selected race data
        /// </summary>
        private RecipientViewModel GetApprovalModel(PostedCaregiverApproval postedCaregiverApproval)
        {
            // setup properties
            var model = new RecipientViewModel();
            var selectedCaregiverApproval = new List<CaregiverApprovals>();
            var postedApprovalIds = new string[0];
            if (postedCaregiverApproval == null) postedCaregiverApproval = new PostedCaregiverApproval();

            // if a view model array of posted race ids exists
            // and is not empty,save selected ids
            if (postedCaregiverApproval.ApprovalCodes != null && postedCaregiverApproval.ApprovalCodes.Any())
            {
                postedApprovalIds = postedCaregiverApproval.ApprovalCodes;
            }

            // if there are any selected ids saved, create a list of race
            if (postedApprovalIds.Any())
            {
                selectedCaregiverApproval = GetAllApprovals()
                 .Where(x => postedApprovalIds.Any(s => x.ApprovalCode.ToString().Equals(s)))
                 .ToList();
            }

            //setup a view model
            model.AvailableApprovals = GetAllApprovals().ToList();
            model.SelectedApprovals = selectedCaregiverApproval;
            model.postedCaregiverApproval = postedCaregiverApproval;
            return model;
        }


        private string GetCaregiverApprovalList(RecipientViewModel RecipVM)
        {
            var selectedApproval = new List<CaregiverApprovals>();
            var postedApprovalIds = new string[0];
            var postedApproval = new PostedCaregiverApproval();
            postedApproval = RecipVM.postedCaregiverApproval;
            // if a view model array of posted race ids exists
            // and is not empty,save selected ids
            if (postedApproval != null && postedApproval.ApprovalCodes != null && postedApproval.ApprovalCodes.Any())
            {
                postedApprovalIds = postedApproval.ApprovalCodes;
            }

            // if there are any selected ids saved, create a list of approvals
            if (postedApprovalIds.Any())
            {
                selectedApproval = GetAllApprovals()
                 .Where(x => postedApprovalIds.Any(s => x.ApprovalCode.ToString().Equals(s)))
                 .ToList();
            }

            //setup a view model
            RecipVM.AvailableApprovals = GetAllApprovals().ToList();
            RecipVM.SelectedApprovals = selectedApproval;
            RecipVM.postedCaregiverApproval = postedApproval;


            string ApprovalList = "";
            if(postedApproval != null)
            {
                foreach (string ApprovalId in postedApproval.ApprovalCodes)
                {
                    if (ApprovalId != null)
                    {
                        ApprovalList = ApprovalList + ApprovalId + ",";
                    }

                }
            }
            
            if(ApprovalList.Length > 0)
            {
                ApprovalList = ApprovalList.Substring(0, ApprovalList.Length - 1);
            }
            
            //End race
            return ApprovalList;

        }

        #endregion

        #region Get Approval from DataSet
        private List<CaregiverApprovals> GetApprovalFromDataSet(int NSID)
        {
            List<CaregiverApprovals> lsApprovals = new List<CaregiverApprovals>();

            try
            {
                DataSet ApprovalDS = getProxy().GetCaregiverApproval(NSID);
                for (var index = 0; index < ApprovalDS.Tables[0].Rows.Count; index++)
                {
                    if ((int)ApprovalDS.Tables[0].Rows[index]["CaregiverApproval"] < 100)
                    {
                        lsApprovals.Add(new CaregiverApprovals() { ApprovalCode = (int)ApprovalDS.Tables[0].Rows[index]["CaregiverApproval"], ApprovalDescription = ApprovalDS.Tables[0].Rows[index]["CaregiverApprovalDescription"].ToString(), IsSelected = true });
                    }
                }

            }
            catch
            {
                throw;
            }

            return lsApprovals;
        }

        #endregion

        #region Get Approval Other from DataSet
        private string GetApprovalOtherFromDataSet(int NSID)
        {
            string approvalOther = "";

            try
            {
                DataSet ApprovalOtherDS = getProxy().GetCaregiverApproval(NSID);

                if(ApprovalOtherDS.Tables[0].Rows.Count > 0)
                { approvalOther = ApprovalOtherDS.Tables[0].Rows[0]["CaregiverApprovalOther"].ToString(); }
                


            }
            catch
            {
                throw;
            }

            return approvalOther;
        }

        #endregion

        #region Get Navigation Cycle from DataSet
        private tNavigationCycle GetNavigationCycleFromDataSet(int NSRecipientID)
        {
            tNavigationCycle tNavigationCycle = new tNavigationCycle();
            try
            {
                DataSet cyclesDS = getProxy().GetNavigationCycle(1, 0, NSRecipientID);
                for (var index = 0; index < cyclesDS.Tables[0].Rows.Count; index++)
                {

                    tNavigationCycle = new tNavigationCycle()
                    {
                        NavigationCycleID = (int)cyclesDS.Tables[0].Rows[index]["NavigationCycleID"],
                        FK_RecipientID = (int)cyclesDS.Tables[0].Rows[index]["FK_RecipientID"],
                        FK_ProviderID = Convert.IsDBNull(cyclesDS.Tables[0].Rows[index]["FK_ProviderID"]) ? 0 : (int?)cyclesDS.Tables[0].Rows[index]["FK_ProviderID"],
                        FK_ReferralID = Convert.IsDBNull(cyclesDS.Tables[0].Rows[index]["FK_ReferralID"]) ? 0 : (int?)cyclesDS.Tables[0].Rows[index]["FK_ReferralID"],
                        CancerSite = (int)cyclesDS.Tables[0].Rows[index]["CancerSite"],
                        NSProblem = (int)cyclesDS.Tables[0].Rows[index]["NSProblem"],
                        HealthProgram = (int)cyclesDS.Tables[0].Rows[index]["HealthProgram"],
                        HealthInsuranceStatus = (int)cyclesDS.Tables[0].Rows[index]["HealthInsuranceStatus"],
                        HealthInsurancePlan = Convert.IsDBNull(cyclesDS.Tables[0].Rows[index]["HealthInsurancePlan"]) ? 0 : (int?)cyclesDS.Tables[0].Rows[index]["HealthInsurancePlan"],
                        HealthInsurancePlanNumber = cyclesDS.Tables[0].Rows[index]["HealthInsurancePlanNumber"].ToString(),
                        ContactPrefDays = cyclesDS.Tables[0].Rows[index]["ContactPrefDays"].ToString(),
                        ContactPrefHours = cyclesDS.Tables[0].Rows[index]["ContactPrefHours"].ToString(),
                        ContactPrefHoursOther = cyclesDS.Tables[0].Rows[index]["ContactPrefHoursOther"].ToString(),
                        ContactPrefType = Convert.IsDBNull(cyclesDS.Tables[0].Rows[index]["ContactPrefType"]) ? 0 : (int?)cyclesDS.Tables[0].Rows[index]["ContactPrefType"],
                        SRIndentifierCodeGenerated = Convert.IsDBNull(cyclesDS.Tables[0].Rows[index]["SRIndentifierCodeGenerated"]) ? false : (bool?)cyclesDS.Tables[0].Rows[index]["SRIndentifierCodeGenerated"],
                        FK_NavigatorID = (int)cyclesDS.Tables[0].Rows[index]["FK_NavigatorID"],
                        xtag = cyclesDS.Tables[0].Rows[index]["xtag"].ToString(),
                        DateCreated = Convert.IsDBNull(cyclesDS.Tables[0].Rows[index]["DateCreated"]) ? DateTime.MinValue : (DateTime)cyclesDS.Tables[0].Rows[index]["DateCreated"],
                        CreatedBy = cyclesDS.Tables[0].Rows[index]["CreatedBy"].ToString(),
                        LastUpdated = Convert.IsDBNull(cyclesDS.Tables[0].Rows[index]["LastUpdated"]) ? DateTime.MinValue : (DateTime)cyclesDS.Tables[0].Rows[index]["LastUpdated"],
                        UpdatedBy = cyclesDS.Tables[0].Rows[index]["UpdatedBy"].ToString()
                    };
                }

            }
            catch
            {
                throw;
            }

            return tNavigationCycle;
        }

        #endregion

        #region Get NS Provider
        private tNSProvider GetNSProvider(int NSProviderID)
        {
            tNSProvider tNSProvider = new tNSProvider();
            try
            {
                DataSet providersDS = getProxy().SelectNSProvider(NSProviderID);
                tNSProvider = new tNSProvider()
                {
                    NSProviderID = (int)providersDS.Tables[0].Rows[0]["NSProviderID"],
                    PCPName = providersDS.Tables[0].Rows[0]["PCPName"].ToString(),
                    EWCPCP = Convert.IsDBNull(providersDS.Tables[0].Rows[0]["EWCPCP"]) ? false : (bool)providersDS.Tables[0].Rows[0]["EWCPCP"],
                    PCP_NPI = providersDS.Tables[0].Rows[0]["PCP_NPI"].ToString(),
                    PCP_Owner = providersDS.Tables[0].Rows[0]["PCP_Owner"].ToString(),
                    PCP_Location = providersDS.Tables[0].Rows[0]["PCP_Location"].ToString(),
                    PCPAddress1 = providersDS.Tables[0].Rows[0]["PCPAddress1"].ToString(),
                    PCPAddress2 = providersDS.Tables[0].Rows[0]["PCPAddress2"].ToString(),
                    PCPCity = providersDS.Tables[0].Rows[0]["PCPCity"].ToString(),
                    PCPState = providersDS.Tables[0].Rows[0]["PCPState"].ToString(),
                    PCPZip = providersDS.Tables[0].Rows[0]["PCPZip"].ToString(),
                    PCPContactFName = providersDS.Tables[0].Rows[0]["PCPContactFName"].ToString(),
                    PCPContactLName = providersDS.Tables[0].Rows[0]["PCPContactLName"].ToString(),
                    PCPContactTitle = providersDS.Tables[0].Rows[0]["PCPContactTitle"].ToString(),
                    PCPContactTelephone = providersDS.Tables[0].Rows[0]["PCPContactTelephone"].ToString(),
                    PCPContactEmail = providersDS.Tables[0].Rows[0]["PCPContactEmail"].ToString(),
                    Medical = Convert.IsDBNull(providersDS.Tables[0].Rows[0]["Medical"]) ? false : (bool)providersDS.Tables[0].Rows[0]["Medical"],
                    MedicalSpecialty = Convert.IsDBNull(providersDS.Tables[0].Rows[0]["MedicalSpecialty"]) ? 0 : (int)providersDS.Tables[0].Rows[0]["MedicalSpecialty"],
                    MedicalSpecialtyOther = providersDS.Tables[0].Rows[0]["MedicalSpecialtyOther"].ToString(),
                    ManagedCarePlan = Convert.IsDBNull(providersDS.Tables[0].Rows[0]["ManagedCarePlan"]) ? 0 : (int?)providersDS.Tables[0].Rows[0]["ManagedCarePlan"],
                    ManagedCarePlanOther = providersDS.Tables[0].Rows[0]["ManagedCarePlanOther"].ToString(),
                    xtag = providersDS.Tables[0].Rows[0]["xtag"].ToString(),
                    DateCreated = Convert.IsDBNull(providersDS.Tables[0].Rows[0]["DateCreated"]) ? DateTime.MinValue : (DateTime)providersDS.Tables[0].Rows[0]["DateCreated"],
                    CreatedBy = providersDS.Tables[0].Rows[0]["CreatedBy"].ToString(),
                    LastUpdated = Convert.IsDBNull(providersDS.Tables[0].Rows[0]["LastUpdated"]) ? DateTime.MinValue : (DateTime)providersDS.Tables[0].Rows[0]["LastUpdated"],
                    UpdatedBy = providersDS.Tables[0].Rows[0]["UpdatedBy"].ToString()
                };


            }
            catch
            {
                throw;
            }

            return tNSProvider;
        }

        #endregion
        
        #region "Ported code from Navigation Cycles Controller"

        private ActionResult Save([Bind(Exclude = "NSProviderID")] RecipientViewModel CycleVM, int NSID, int NavigatorID)
        {
            int NavigationCycleID = 0;
            int NSProviderID = 0;

            if (ModelState.IsValid)
            {
                //Weekdays 
                var WeekdaysList = GetWeekdayList(CycleVM);
                CycleVM.tNavigationCycle.ContactPrefDays = WeekdaysList.ToString();

                //Times 
                var TimesList = GetTimeList(CycleVM);
                CycleVM.tNavigationCycle.ContactPrefHours = TimesList.ToString();

                //CreatedBy
                CycleVM.tNavigationCycle.CreatedBy = "";

                //Get Navigator ID

                CycleVM.tNavigationCycle.FK_NavigatorID = NavigatorID;

                //Get Recipient ID
                CycleVM.tNavigationCycle.FK_RecipientID = NSID;

                //Provider CreatedBy
                CycleVM.tNSProvider.CreatedBy = "";


                try
                {
                    if (CycleVM.tNSProvider.PCPName != "" 
                        && CycleVM.tNSProvider.PCPName != null 
                        && CycleVM.tNSProvider.PCP_NPI != "" 
                        && CycleVM.tNSProvider.PCP_NPI != null)
                    {
                        //Update Provider Record
                        NSProviderID = getProxy().InsertNSProvider(CycleVM.tNSProvider.PCPName,
                            CycleVM.tNSProvider.EWCPCP,
                            CycleVM.tNSProvider.PCP_NPI, CycleVM.tNSProvider.PCP_Owner,
                            CycleVM.tNSProvider.PCP_Location, CycleVM.tNSProvider.PCPAddress1,
                            CycleVM.tNSProvider.PCPAddress2, CycleVM.tNSProvider.PCPCity,
                            CycleVM.tNSProvider.PCPState, CycleVM.tNSProvider.PCPZip,
                            CycleVM.tNSProvider.PCPContactFName, CycleVM.tNSProvider.PCPContactLName,
                            CycleVM.tNSProvider.PCPContactTitle, CycleVM.tNSProvider.PCPContactTelephone,
                            CycleVM.tNSProvider.PCPContactEmail, CycleVM.tNSProvider.Medical,
                            CycleVM.tNSProvider.MedicalSpecialty, CycleVM.tNSProvider.MedicalSpecialtyOther,
                            CycleVM.tNSProvider.ManagedCarePlan, CycleVM.tNSProvider.ManagedCarePlanOther);

                        //Get inserted Provider ID
                        CycleVM.tNavigationCycle.FK_ProviderID = NSProviderID;
                    }
                    //Insert Navigation Cycle

                    NavigationCycleID = getProxy().InsertNavigationCycle(CycleVM.tNavigationCycle.FK_RecipientID
                                                , CycleVM.tNavigationCycle.FK_ProviderID
                                                , CycleVM.tNavigationCycle.FK_ReferralID
                                                , CycleVM.tNavigationCycle.CancerSite
                                                , CycleVM.tNavigationCycle.NSProblem
                                                , CycleVM.tNavigationCycle.HealthProgram
                                                , CycleVM.tNavigationCycle.HealthInsuranceStatus
                                                , CycleVM.tNavigationCycle.HealthInsurancePlan
                                                , CycleVM.tNavigationCycle.HealthInsurancePlanNumber
                                                , CycleVM.tNavigationCycle.ContactPrefDays
                                                , CycleVM.tNavigationCycle.ContactPrefHours
                                                , CycleVM.tNavigationCycle.ContactPrefHoursOther
                                                , CycleVM.tNavigationCycle.ContactPrefType
                                                , CycleVM.tNavigationCycle.SRIndentifierCodeGenerated
                                                , CycleVM.tNavigationCycle.FK_NavigatorID);


                }
                catch (System.Data.Entity.Validation.DbEntityValidationException e)
                {
                    foreach (var eve in e.EntityValidationErrors)
                    {
                        Console.WriteLine("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                            eve.Entry.Entity.GetType().Name, eve.Entry.State);
                        foreach (var ve in eve.ValidationErrors)
                        {
                            Console.WriteLine("- Property: \"{0}\", Error: \"{1}\"",
                                ve.PropertyName, ve.ErrorMessage);
                        }
                    }
                    throw; //Errors.PublishException(e, GetAppName(false), GetVersion());
                }

                TempData["FeedbackMessage"] = "Your data was saved successfully.";

            }
            else
            {
                var allErrors = ModelState.Values.SelectMany(v => v.Errors);
                //Declare Navigation Cycle View Model
                var tnavigationcycleVM = new RecipientViewModel();
                var selectedWeekday = new List<WeekdayContactPreference>();
                var selectedTime = new List<TimeContactPreference>();

                try
                {
                    //Weekday setup
                    tnavigationcycleVM.AvailableWeekday = GetAllWeekdays().ToList();
                    tnavigationcycleVM.SelectedWeekday = selectedWeekday;

                    //Time setup
                    tnavigationcycleVM.AvailableTime = GetAllTimes().ToList();
                    tnavigationcycleVM.SelectedTime = selectedTime;

                }
                catch (Exception e)
                {
                    throw; //Errors.PublishException(e, GetAppName(false), GetVersion());
                }

                return View(tnavigationcycleVM);
            }

            return RedirectToAction("Index", "Enrollment");

        }

        // Merged edit
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(RecipientViewModel RecipVM)//[Bind(Include = "NavigationCycleID,FK_RecipientID,FK_ReferralID,CancerSite,NSProblem,HealthProgram,HealthInsuranceStatus,HealthInsurancePlan,HealthInsurancePlanNumber,ContactPrefDays,ContactPrefHours,ContactPrefHoursOther,ContactPrefType,SRIndentifierCodeGenerated,FK_NavigatorID,NavigatorTransfer,NavigatorTransferDt,NavigatorTransferReason,NewNavigatorID,xtag,DateCreated,CreatedBy,LastUpdated,UpdatedBy")] tNavigationCycle tNavigationCycle)
        {
            int NavigatorID = 0;

            if (ModelState.IsValid)
            {
                //Race 
                var RaceList = GetRaceList(RecipVM);
                //Caregiver Approvals 
                var ApprovalList = GetCaregiverApprovalList(RecipVM);

                RecipVM.tServiceRecipient.CreatedBy = "";

                //Weekdays -- NavigationCyclesController
                var WeekdaysList = GetWeekdayList(RecipVM);
                RecipVM.tNavigationCycle.ContactPrefDays = WeekdaysList;

                //Times -- NavigationCyclesController
                var TimesList = GetTimeList(RecipVM);
                RecipVM.tNavigationCycle.ContactPrefHours = TimesList;

                //CreatedBy -- NavigationCyclesController 
                RecipVM.tNavigationCycle.CreatedBy = "";
                RecipVM.tNavigationCycle.FK_ProviderID = RecipVM.tNSProvider.NSProviderID;

                try
                {
                    //Get Navigator ID
                    RecipVM.tServiceRecipient.FK_NavigatorID = GetNavigatorID();
                    NavigatorID = GetNavigatorID();

                    // NavigationCyclesController 
                    RecipVM.tNavigationCycle.FK_NavigatorID = NavigatorID;
                }
                catch (Exception e)
                {
                    throw;
                }

                //Provider CreatedBy  -- NavigationCyclesController 
                RecipVM.tNSProvider.CreatedBy = "";

                try
                {

                    int retValue = getProxy().UpdateServiceRecipient(RecipVM.tServiceRecipient.NSRecipientID,
                        RecipVM.tServiceRecipient.FK_NavigatorID,
                        RecipVM.tServiceRecipient.FK_RecipID,
                        RecipVM.tServiceRecipient.LastName,
                        RecipVM.tServiceRecipient.FirstName,
                        RecipVM.tServiceRecipient.MiddleInitial,
                        RecipVM.tServiceRecipient.DOB,
                        RecipVM.tServiceRecipient.AddressNotAvailable,
                        RecipVM.tServiceRecipient.Address1,
                        RecipVM.tServiceRecipient.Address2,
                        RecipVM.tServiceRecipient.City,
                        RecipVM.tServiceRecipient.State,
                        RecipVM.tServiceRecipient.Zip,
                        RecipVM.tServiceRecipient.Phone1,
                        RecipVM.tServiceRecipient.P1PhoneType,
                        RecipVM.tServiceRecipient.P1PersonalMessage,
                        RecipVM.tServiceRecipient.Phone2,
                        RecipVM.tServiceRecipient.P2PhoneType,
                        RecipVM.tServiceRecipient.P2PersonalMessage,
                        RecipVM.tServiceRecipient.MotherMaidenName,
                        RecipVM.tServiceRecipient.Email,
                        RecipVM.tServiceRecipient.SSN,
                        RecipVM.tServiceRecipient.ImmigrantStatus,
                        RecipVM.tServiceRecipient.CountryOfOrigin,
                        RecipVM.tServiceRecipient.Gender,
                        RecipVM.tServiceRecipient.Ethnicity,
                        RecipVM.tServiceRecipient.PrimaryLanguage,
                        RecipVM.tServiceRecipient.AgreeToSpeakEnglish,
                        RecipVM.tServiceRecipient.CaregiverName,
                        RecipVM.tServiceRecipient.Relationship,
                        RecipVM.tServiceRecipient.RelationshipOther,
                        RecipVM.tServiceRecipient.CaregiverPhone,
                        RecipVM.tServiceRecipient.CaregiverPhoneType,
                        RecipVM.tServiceRecipient.CaregiverPhonePersonalMessage,
                        RecipVM.tServiceRecipient.CaregiverEmail,
                        RecipVM.tServiceRecipient.CaregiverContactPreference,
                        RecipVM.tServiceRecipient.Comments);


                    int NSID = RecipVM.tServiceRecipient.NSRecipientID;

                    if (RecipVM.tRace.RaceOther == null)
                    {
                        RecipVM.tRace.RaceOther = "";
                    }
                    getProxy().SaveRace(NSID, RaceList, RecipVM.tRace.RaceOther);

                    if (RecipVM.tCaregiverApprovals.CaregiverApprovalOther == null)
                    {
                        RecipVM.tCaregiverApprovals.CaregiverApprovalOther = "";
                    }
                    getProxy().SaveCaregiverApproval(NSID, ApprovalList, RecipVM.tCaregiverApprovals.CaregiverApprovalOther);

                    //Update Provider Record -- NavigationCyclesController 
                    if (RecipVM.tNavigationCycle.FK_ProviderID != null && RecipVM.tNavigationCycle.FK_ProviderID != 0)
                    {
                        getProxy().UpdateNSProvider(RecipVM.tNSProvider.NSProviderID, RecipVM.tNSProvider.PCPName, RecipVM.tNSProvider.EWCPCP,
                            RecipVM.tNSProvider.PCP_NPI, RecipVM.tNSProvider.PCP_Owner, RecipVM.tNSProvider.PCP_Location, RecipVM.tNSProvider.PCPAddress1, RecipVM.tNSProvider.PCPAddress2,
                        RecipVM.tNSProvider.PCPCity, RecipVM.tNSProvider.PCPState, RecipVM.tNSProvider.PCPZip, RecipVM.tNSProvider.PCPContactFName, RecipVM.tNSProvider.PCPContactLName, RecipVM.tNSProvider.PCPContactTitle,
                        RecipVM.tNSProvider.PCPContactTelephone, RecipVM.tNSProvider.PCPContactEmail, RecipVM.tNSProvider.Medical, RecipVM.tNSProvider.MedicalSpecialty, RecipVM.tNSProvider.MedicalSpecialtyOther,
                        RecipVM.tNSProvider.ManagedCarePlan, RecipVM.tNSProvider.ManagedCarePlanOther);


                    }
                    else if (RecipVM.tNSProvider.PCPName != "" && RecipVM.tNSProvider.PCPName != null && RecipVM.tNSProvider.PCP_NPI != "" && RecipVM.tNSProvider.PCP_NPI != null)
                    {
                        //Insert Provider Record
                        RecipVM.tNavigationCycle.FK_ProviderID = getProxy().InsertNSProvider(RecipVM.tNSProvider.PCPName, RecipVM.tNSProvider.EWCPCP,
                            RecipVM.tNSProvider.PCP_NPI, RecipVM.tNSProvider.PCP_Owner,
                            RecipVM.tNSProvider.PCP_Location, RecipVM.tNSProvider.PCPAddress1, RecipVM.tNSProvider.PCPAddress2,
                            RecipVM.tNSProvider.PCPCity, RecipVM.tNSProvider.PCPState, RecipVM.tNSProvider.PCPZip, RecipVM.tNSProvider.PCPContactFName, RecipVM.tNSProvider.PCPContactLName, RecipVM.tNSProvider.PCPContactTitle,
                            RecipVM.tNSProvider.PCPContactTelephone, RecipVM.tNSProvider.PCPContactEmail, RecipVM.tNSProvider.Medical, RecipVM.tNSProvider.MedicalSpecialty, RecipVM.tNSProvider.MedicalSpecialtyOther,
                            RecipVM.tNSProvider.ManagedCarePlan, RecipVM.tNSProvider.ManagedCarePlanOther);

                    }

                    //Update Navigation Cycle -- NavigationCyclesController 
                    getProxy().UpdateNavigationCycle(RecipVM.tNavigationCycle.NavigationCycleID, RecipVM.tNavigationCycle.FK_RecipientID, RecipVM.tNavigationCycle.FK_ProviderID, RecipVM.tNavigationCycle.FK_ReferralID, RecipVM.tNavigationCycle.CancerSite,
                    RecipVM.tNavigationCycle.NSProblem, RecipVM.tNavigationCycle.HealthProgram, RecipVM.tNavigationCycle.HealthInsuranceStatus, RecipVM.tNavigationCycle.HealthInsurancePlan, RecipVM.tNavigationCycle.HealthInsurancePlanNumber,
                    RecipVM.tNavigationCycle.ContactPrefDays, RecipVM.tNavigationCycle.ContactPrefHours, RecipVM.tNavigationCycle.ContactPrefHoursOther, RecipVM.tNavigationCycle.ContactPrefType, RecipVM.tNavigationCycle.SRIndentifierCodeGenerated,
                    RecipVM.tNavigationCycle.FK_NavigatorID);
                }
                catch (System.Data.Entity.Validation.DbEntityValidationException e)
                {
                    foreach (var eve in e.EntityValidationErrors)
                    {
                        Console.WriteLine("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                            eve.Entry.Entity.GetType().Name, eve.Entry.State);
                        foreach (var ve in eve.ValidationErrors)
                        {
                            Console.WriteLine("- Property: \"{0}\", Error: \"{1}\"",
                                ve.PropertyName, ve.ErrorMessage);
                        }
                    }
                    throw; //Errors.PublishException(e, GetAppName(false), GetVersion()); 
                }

                TempData["FeedbackMessage"] = "Your changes were saved successfully.";
                return View(RecipVM);

            }
            else
            {
                var allErrors = ModelState.Values.SelectMany(v => v.Errors);

                tServiceRecipient tServiceRecipient = GetRecipientFromDataSet(RecipVM.tServiceRecipient.NSRecipientID);

                RecipientViewModel recipVM = new RecipientViewModel();

                tRace tRace = new tRace();

                tCaregiverApprovals tCaregiverApprovals = new tCaregiverApprovals();

                List<Race> ls = new List<Race>();
                ls = GetRaceFromDataSet(RecipVM.tServiceRecipient.NSRecipientID);

                recipVM.RaceAsianCode = GetAsianRaceFromDataSet(RecipVM.tServiceRecipient.NSRecipientID);

                recipVM.RacePacIslanderCode = GetPacIslanderRaceFromDataSet(RecipVM.tServiceRecipient.NSRecipientID);

                tRace.RaceOther = GetRaceOtherFromDataSet(RecipVM.tServiceRecipient.NSRecipientID);

                recipVM.SelectedRace = ls;
                recipVM.AvailableRace = GetAll().ToList();

                List<CaregiverApprovals> ls1 = new List<CaregiverApprovals>();
                ls1 = GetApprovalFromDataSet(RecipVM.tServiceRecipient.NSRecipientID);

                tCaregiverApprovals.CaregiverApprovalOther = GetApprovalOtherFromDataSet(RecipVM.tServiceRecipient.NSRecipientID);

                recipVM.SelectedApprovals = ls1;
                recipVM.AvailableApprovals = GetAllApprovals().ToList();

                recipVM.tServiceRecipient = tServiceRecipient;
                recipVM.tRace = tRace;
                recipVM.tCaregiverApprovals = tCaregiverApprovals;                

                // -- NavigationCyclesController 
                tNavigationCycle tnavigationCycle = GetNavigationCycle(RecipVM.tNavigationCycle.NavigationCycleID);

                // -- NavigationCyclesController 
                recipVM.tNavigationCycle = tnavigationCycle;

                //Weekdays -- NavigationCyclesController 
                PostedWeekday postedWeekday = new PostedWeekday();

                //-- NavigationCyclesController 
                string weekdayString = recipVM.tNavigationCycle.ContactPrefDays;
                string[] weekdays = weekdayString.Split(',');
                postedWeekday.WeekdayIds = weekdays;

                //Set up the weekdays in View Model -- NavigationCyclesController 
                recipVM.SelectedWeekday = GetWeekdaySelected(postedWeekday);
                recipVM.postedWeekday = postedWeekday;
                recipVM.AvailableWeekday = GetAllWeekdays().ToList();

                //Times -- NavigationCyclesController 
                PostedTime postedTime = new PostedTime();

                string timeString = recipVM.tNavigationCycle.ContactPrefHours;
                string[] times = timeString.Split(',');
                postedTime.TimeIds = times;

                //Set up the weekdays in View Model -- NavigationCyclesController 
                recipVM.SelectedTime = GetTimeSelected(postedTime);
                recipVM.postedTime = postedTime;
                recipVM.AvailableTime = GetAllTimes().ToList();

                //Provider -- NavigationCyclesController 
                try
                {
                    if (tnavigationCycle.FK_ProviderID != null && tnavigationCycle.FK_ProviderID != 0)
                    {
                        recipVM.tNSProvider = GetNSProvider((int)tnavigationCycle.FK_ProviderID);
                    }
                    else
                    {
                        //Save 0 in NSPRoviderID
                        tNSProvider tnsProvider = new tNSProvider();
                        tnsProvider.NSProviderID = 0;
                        recipVM.tNSProvider = tnsProvider;
                    }
                }
                catch (Exception e)
                {
                    throw; //Errors.PublishException(e, GetAppName(false), GetVersion());
                }

                return View(recipVM);

            }
        }


        #region Get Navigation Cycle
        private tNavigationCycle GetNavigationCycle(int NavigationCycleID)
        {
            tNavigationCycle tNavigationCycle = new tNavigationCycle();
            try
            {
                DataSet cyclesDS = getProxy().GetNavigationCycle(0, NavigationCycleID, 0);
                for (var index = 0; index < cyclesDS.Tables[0].Rows.Count; index++)
                {

                    tNavigationCycle = new tNavigationCycle()
                    {
                        NavigationCycleID = (int)cyclesDS.Tables[0].Rows[index]["NavigationCycleID"],
                        FK_RecipientID = (int)cyclesDS.Tables[0].Rows[index]["FK_RecipientID"],
                        FK_ProviderID = Convert.IsDBNull(cyclesDS.Tables[0].Rows[index]["FK_ProviderID"]) ? 0 : (int?)cyclesDS.Tables[0].Rows[index]["FK_ProviderID"],
                        FK_ReferralID = Convert.IsDBNull(cyclesDS.Tables[0].Rows[index]["FK_ReferralID"]) ? 0 : (int?)cyclesDS.Tables[0].Rows[index]["FK_ReferralID"],
                        CancerSite = (int)cyclesDS.Tables[0].Rows[index]["CancerSite"],
                        NSProblem = (int)cyclesDS.Tables[0].Rows[index]["NSProblem"],
                        HealthProgram = (int)cyclesDS.Tables[0].Rows[index]["HealthProgram"],
                        HealthInsuranceStatus = (int)cyclesDS.Tables[0].Rows[index]["HealthInsuranceStatus"],
                        HealthInsurancePlan = Convert.IsDBNull(cyclesDS.Tables[0].Rows[index]["HealthInsurancePlan"]) ? 0 : (int?)cyclesDS.Tables[0].Rows[index]["HealthInsurancePlan"],
                        HealthInsurancePlanNumber = cyclesDS.Tables[0].Rows[index]["HealthInsurancePlanNumber"].ToString(),
                        ContactPrefDays = cyclesDS.Tables[0].Rows[index]["ContactPrefDays"].ToString(),
                        ContactPrefHours = cyclesDS.Tables[0].Rows[index]["ContactPrefHours"].ToString(),
                        ContactPrefHoursOther = cyclesDS.Tables[0].Rows[index]["ContactPrefHoursOther"].ToString(),
                        ContactPrefType = Convert.IsDBNull(cyclesDS.Tables[0].Rows[index]["ContactPrefType"]) ? 0 : (int?)cyclesDS.Tables[0].Rows[index]["ContactPrefType"],
                        SRIndentifierCodeGenerated = Convert.IsDBNull(cyclesDS.Tables[0].Rows[index]["SRIndentifierCodeGenerated"]) ? false : (bool?)cyclesDS.Tables[0].Rows[index]["SRIndentifierCodeGenerated"],
                        FK_NavigatorID = (int)cyclesDS.Tables[0].Rows[index]["FK_NavigatorID"],
                        xtag = cyclesDS.Tables[0].Rows[index]["xtag"].ToString(),
                        DateCreated = Convert.IsDBNull(cyclesDS.Tables[0].Rows[index]["DateCreated"]) ? DateTime.MinValue : (DateTime)cyclesDS.Tables[0].Rows[index]["DateCreated"],
                        CreatedBy = cyclesDS.Tables[0].Rows[index]["CreatedBy"].ToString(),
                        LastUpdated = Convert.IsDBNull(cyclesDS.Tables[0].Rows[index]["LastUpdated"]) ? DateTime.MinValue : (DateTime)cyclesDS.Tables[0].Rows[index]["LastUpdated"],
                        UpdatedBy = cyclesDS.Tables[0].Rows[index]["UpdatedBy"].ToString()
                    };
                }

            }
            catch
            {
                throw;
            }

            return tNavigationCycle;
        }

        #endregion

        #region Get Weekday Selected
        /// <summary>
        /// for setup view model, after post the user selected weekday data
        /// </summary>
        private List<WeekdayContactPreference> GetWeekdaySelected(PostedWeekday postedWeekday)
        {
            // setup properties
            var model = new RecipientViewModel();
            var selectedWeekday = new List<WeekdayContactPreference>();
            var postedWeekdayIds = new string[0];
            if (null == postedWeekday ) postedWeekday = new PostedWeekday();

            // if a view model array of posted weekday ids exists
            // and is not empty,save selected ids
            if (null != postedWeekday.WeekdayIds && postedWeekday.WeekdayIds.Any())
            {
                postedWeekdayIds = postedWeekday.WeekdayIds;
            }

            // if there are any selected ids saved, create a list of weekday
            if (postedWeekdayIds.Any())
            {
                selectedWeekday = GetAllWeekdays()
                 .Where(x => postedWeekdayIds.Any(s => x.WeekdayCode.ToString().Equals(s)))
                 .ToList();
            }

            ////setup a view model
            //model.AvailableWeekday = GetAllWeekdays().ToList();
            //model.SelectedWeekday = selectedWeekday;
            //model.postedWeekday = postedWeekday;
            return selectedWeekday;
        }
        #endregion

        #region Get Weekday List
        //private string GetWeekdayList(NavigationCycleViewModel CycleVM)
        private string GetWeekdayList(RecipientViewModel CycleVM)
        {
            var selectedWeekday = new List<WeekdayContactPreference>();
            var postedWeekdayIds = new string[0];
            var postedWeekday = new PostedWeekday();
            postedWeekday = CycleVM.postedWeekday;
            // if a view model array of posted weekday ids exists
            // and is not empty,save selected ids
            if (postedWeekday != null && postedWeekday.WeekdayIds != null && postedWeekday.WeekdayIds.Any())
            {
                postedWeekdayIds = postedWeekday.WeekdayIds;
            }

            // if there are any selected ids saved, create a list of weekdays
            if (postedWeekdayIds.Any())
            {
                selectedWeekday = GetAllWeekdays()
                 .Where(x => postedWeekdayIds.Any(s => x.WeekdayCode.ToString().Equals(s)))
                 .ToList();
            }

            //setup a view model
            CycleVM.AvailableWeekday = GetAllWeekdays().ToList();
            CycleVM.SelectedWeekday = selectedWeekday;
            CycleVM.postedWeekday = postedWeekday;


            string WeekdayList = "";
            if (postedWeekday != null)
            {
                foreach (string WeekdayId in postedWeekday.WeekdayIds)
                {
                    if (WeekdayId != null)
                    {
                        WeekdayList = WeekdayList + WeekdayId + ",";
                    }

                }
                WeekdayList = WeekdayList.Substring(0, WeekdayList.Length - 1);
            }

            //End weekday
            return WeekdayList;

        }

        public static IEnumerable<WeekdayContactPreference> GetAllWeekdays()
        {
            List<WeekdayContactPreference> ls = new List<WeekdayContactPreference>();

            ls.Add(new WeekdayContactPreference() { WeekdayName = "Monday", WeekdayCode = 1 });
            ls.Add(new WeekdayContactPreference() { WeekdayName = "Tuesday", WeekdayCode = 2 });
            ls.Add(new WeekdayContactPreference() { WeekdayName = "Wednesday", WeekdayCode = 3 });
            ls.Add(new WeekdayContactPreference() { WeekdayName = "Thursday", WeekdayCode = 4 });
            ls.Add(new WeekdayContactPreference() { WeekdayName = "Friday", WeekdayCode = 5 });
            ls.Add(new WeekdayContactPreference() { WeekdayName = "Saturday", WeekdayCode = 6 });


            return ls;
        }
        #endregion

        #region Get All Times
        /// <summary>
        /// for get all Times
        /// </summary>
        public static IEnumerable<TimeContactPreference> GetAllTimes()
        {
            List<TimeContactPreference> ls = new List<TimeContactPreference>();

            ls.Add(new TimeContactPreference() { TimeName = "7am-9am", TimeCode = 1 });
            ls.Add(new TimeContactPreference() { TimeName = "10am-12pm", TimeCode = 2 });
            ls.Add(new TimeContactPreference() { TimeName = "1pm-3pm", TimeCode = 3 });
            ls.Add(new TimeContactPreference() { TimeName = "4pm-5pm", TimeCode = 4 });
            ls.Add(new TimeContactPreference() { TimeName = "6pm-9pm", TimeCode = 5 });
            ls.Add(new TimeContactPreference() { TimeName = "Other", TimeCode = 6 });


            return ls;
        }

        #endregion

        #region Get Time Selected
        /// <summary>
        /// for setup view model, after post the user selected time data
        /// </summary>
        private List<TimeContactPreference> GetTimeSelected(PostedTime postedTime)
        {
            // setup properties
            var model = new RecipientViewModel();
            var selectedTime = new List<TimeContactPreference>();
            var postedTimeIds = new string[0];
            if (postedTime == null) postedTime = new PostedTime();

            // if a view model array of posted time ids exists
            // and is not empty,save selected ids
            if (postedTime.TimeIds != null && postedTime.TimeIds.Any())
            {
                postedTimeIds = postedTime.TimeIds;
            }

            // if there are any selected ids saved, create a list of times
            if (postedTimeIds.Any())
            {
                selectedTime = GetAllTimes()
                 .Where(x => postedTimeIds.Any(s => x.TimeCode.ToString().Equals(s)))
                 .ToList();
            }

            ////setup a view model
            //model.AvailableTime = GetAllTimes().ToList();
            //model.SelectedTime = selectedTime;
            //model.postedTime = postedTime;
            return selectedTime;
        }
        #endregion

        #region Get Time List
        private string GetTimeList(RecipientViewModel cycleVM)
        {
            var selectedTime = new List<TimeContactPreference>();
            var postedTimeIds = new string[0];
            var postedTime = new PostedTime();
            postedTime = cycleVM.postedTime;
            // if a view model array of posted time ids exists
            // and is not empty,save selected ids
            if (postedTime != null && postedTime.TimeIds != null && postedTime.TimeIds.Any())
            {
                postedTimeIds = postedTime.TimeIds;
            }

            // if there are any selected ids saved, create a list of race
            if (postedTimeIds.Any())
            {
                selectedTime = GetAllTimes()
                 .Where(x => postedTimeIds.Any(s => x.TimeCode.ToString().Equals(s)))
                 .ToList();
            }

            //setup a view model
            cycleVM.AvailableTime = GetAllTimes().ToList();
            cycleVM.SelectedTime = selectedTime;
            cycleVM.postedTime = postedTime;


            string TimeList = "";
            if (postedTime != null)
            {
                foreach (string TimeId in postedTime.TimeIds)
                {
                    if (TimeId != null)
                    {
                        TimeList = TimeList + TimeId + ",";
                    }

                }
                TimeList = TimeList.Substring(0, TimeList.Length - 1);
            }

            //End time
            return TimeList;
        }
        #endregion

        #endregion
    }






}


