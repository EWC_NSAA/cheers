﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using EWC_NSAA.Models;
using EWC_NSAA.ViewModels;
using EWC_NSAA.Common;

namespace EWC_NSAA.Controllers
{
    //[Authorize(Roles = @"EWC_NSAA_Contributors, EWC_NSAA_A")]
    public class EncountersController : BaseController
    {
        // GET: Encounters
        public ActionResult Index(int NSID, int NavigatorID)
        {

            EncounterListViewModel EncounterListVM = new EncounterListViewModel();


            try
            {
                List<tEncounter> encountersList = GetEncounterList(NSID);
                EncounterListVM.Encounters = encountersList;

                List<tServiceRecipient> recipientsList = GetRecipientsFromDataSet(NavigatorID);
                EncounterListVM.Recipients = recipientsList;

            }
            catch (Exception e)
            {
                throw; //Errors.PublishException(e, GetAppName(false), GetVersion());
            }

            return View(EncounterListVM);
        }


        // GET: Encounters/Create
        public ActionResult Create(int NSID, int NavigatorID)
        {
            //NSID = 1233;
            EncounterViewModel EncounterVM = new EncounterViewModel();
            tEncounter tEncounter = new tEncounter();

            List<tEncounter> encountersList = GetEncounterList(NSID);
            if (encountersList.Count > 0)
            {
                tEncounter.EncounterNumber = encountersList.Max(x => x.EncounterNumber) + 1;
            }
            else
            {
                tEncounter.EncounterNumber = 1;
            }
            tEncounter.EncounterDt = DateTime.Today;
            tEncounter.EncounterStartTime = DateTime.Now.ToShortTimeString();
            EncounterVM.tEncounter = tEncounter;

            List<trBarrierType> barrierTypesLookup = new List<trBarrierType>();
            List<trBarrier> barriersLookup = new List<trBarrier>();
            List<tBarrier> barriersList = new List<tBarrier>();
            if (encountersList.Count > 0)
            {
                barriersList.AddRange(GetBarriersFromDataSet(encountersList.FirstOrDefault().EncounterID));
            }
            EncounterVM.BarrierList = barriersList;
            barrierTypesLookup = GetBarrierTypeFromDataSet();
            EncounterVM.BarrierTypes = barrierTypesLookup;

            barriersLookup = GetBarrierFromDataSet();
            EncounterVM.Barriers = barriersLookup;

            return View(EncounterVM);
        }
        //public ActionResult Create(int NSID, int NavigatorID)
        //{
        //    EncounterViewModel EncounterVM = new EncounterViewModel();
        //    tEncounter tEncounter = new tEncounter();

        //    List<tEncounter> encountersList = GetEncounterList(NSID);
        //    if (encountersList.Count > 0)
        //    {
        //        tEncounter.EncounterNumber = encountersList.Max(x => x.EncounterNumber) + 1;
        //    }
        //    else
        //    {
        //        tEncounter.EncounterNumber = 1;
        //    }
        //    tEncounter.EncounterDt = DateTime.Today;
        //    tEncounter.EncounterStartTime = DateTime.Now.ToShortTimeString();

        //    EncounterVM.tEncounter = tEncounter;

        //    return View(EncounterVM);
        //}

        // POST: Encounters/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(EncounterViewModel EncounterVM, int NSID, int NavigatorID)//[Bind(Include = "EncounterID,FK_NavigatorID,FK_RecipientID,FK_NavigationCycleID,EncounterNumber,EncounterDt,EncounterStartTime,EncounterEndTime,EncounterType,MissedAppointment,SvcOutcomeStatus,SvcOutcomeStatusReason,NextAppointmentDt,NextAppointmentTime,NextAppointmentType,ServicesTerminated,ServicesTerminatedDt,ServicesTerminatedReason,Notes,xtag,DateCreated,CreatedBy,LastUpdated,UpdatedBy")] tEncounter tEncounter)
        {

            var EncounterID = 0;
            var BarrierID = 0;

            tBarrier tBarrier = new tBarrier();
            tSolution tSolution = new tSolution();

            if (EncounterVM.SelectedSolution == null)
            {
                TempData["ErrorMessage"] = "A Solution for the Barrier needs to be selected in order to continue. Please try again by selecting a Solution to the Barrier.";
                return RedirectToAction("Create", "Encounters", new { NSID = NSID, NavigatorID = NavigatorID });
            }


            if (ModelState.IsValid)
            {

                //CreatedBy
                EncounterVM.tEncounter.CreatedBy = "";

                //Get Navigator ID
                EncounterVM.tEncounter.FK_NavigatorID = NavigatorID;

                EncounterVM.tEncounter.FK_RecipientID = NSID;

                //EncounterVM.tEncounter.FK_NavigationCycleID = NavigationCycleID;

                if (EncounterVM.tEncounter.NextAppointmentDt == null)
                {
                    EncounterVM.tEncounter.NextAppointmentDt = DateTime.MinValue;
                }

                if (EncounterVM.tEncounter.ServicesTerminatedDt == null)
                {
                    EncounterVM.tEncounter.ServicesTerminatedDt = DateTime.MinValue;
                }

                try
                {
                    //Insert Encounter Record
                    EncounterID = getProxy().InsertEncounter(EncounterVM.tEncounter.FK_NavigatorID,
                        EncounterVM.tEncounter.FK_RecipientID,
                        EncounterVM.tEncounter.EncounterNumber, EncounterVM.tEncounter.EncounterDt,
                        EncounterVM.tEncounter.EncounterStartTime, EncounterVM.tEncounter.EncounterEndTime,
                        EncounterVM.tEncounter.EncounterType, EncounterVM.tEncounter.EncounterReason,
                        EncounterVM.tEncounter.MissedAppointment, EncounterVM.tEncounter.SvcOutcomeStatus,
                        EncounterVM.tEncounter.SvcOutcomeStatusReason, EncounterVM.tEncounter.NextAppointmentDt,
                        EncounterVM.tEncounter.NextAppointmentTime, EncounterVM.tEncounter.NextAppointmentType,
                        EncounterVM.tEncounter.ServicesTerminated, EncounterVM.tEncounter.ServicesTerminatedDt,
                        EncounterVM.tEncounter.ServicesTerminatedReason, EncounterVM.tEncounter.Notes);

                    //Create the tBarrier object
                    tBarrier.BarrierType = EncounterVM.SelectedBarrierType;
                    tBarrier.BarrierAssessed = EncounterVM.SelectedBarrier;
                    tBarrier.CreatedBy = "";
                    tBarrier.FK_EncounterID = EncounterID;
                    tBarrier.FollowUpDt = DateTime.MinValue;

                    //Insert Barrier and get Barrier ID
                    BarrierID = getProxy().InsertBarrier(EncounterID, tBarrier.BarrierType, tBarrier.BarrierAssessed, tBarrier.FollowUpDt);

                    string Solutions = EncounterVM.SelectedSolution;
                    string[] solutions = Solutions.Split(',');

                    string FollowupDt = EncounterVM.SelectedFollowupDt;
                    string[] followupdt = FollowupDt.Split(',');

                    string ImplementationStatus = EncounterVM.SelectedImplementationStatus;
                    string[] implementationstatus = ImplementationStatus.Split(',');


                    for (int i = 0; i < solutions.Count(); i++)
                    {
                        string sol = solutions[i].Substring(solutions[i].IndexOf(":") + 1);
                        string fup = followupdt[i].Substring(followupdt[i].IndexOf(":") + 1);
                        string imp = implementationstatus[i].Substring(implementationstatus[i].IndexOf(":") + 1);

                        if (sol != "") //Solution is checked
                        {

                            tSolution.SolutionOffered = Convert.ToInt32(sol);
                            if (fup != "")
                            { tSolution.FollowUpDt = Convert.ToDateTime(fup); }
                            else
                            { tSolution.FollowUpDt = DateTime.MinValue; }
                            tSolution.SolutionImplementationStatus = Convert.ToInt32(imp);

                            getProxy().InsertSolution(BarrierID, tSolution.SolutionOffered, tSolution.SolutionImplementationStatus, tSolution.FollowUpDt);
                        }
                    }


                }
                catch (System.Data.Entity.Validation.DbEntityValidationException e)
                {
                    foreach (var eve in e.EntityValidationErrors)
                    {
                        Console.WriteLine("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                            eve.Entry.Entity.GetType().Name, eve.Entry.State);
                        foreach (var ve in eve.ValidationErrors)
                        {
                            Console.WriteLine("- Property: \"{0}\", Error: \"{1}\"",
                                ve.PropertyName, ve.ErrorMessage);
                        }
                    }
                    throw; //Errors.PublishException(e, GetAppName(false), GetVersion());
                }

                TempData["FeedbackMessage"] = "Your changes were saved successfully.";

                return RedirectToAction("Edit", "Encounters", new { id = EncounterID });
                //return RedirectToAction("Create", "Encounters", new { NSID = NSID, NavigatorID = NavigatorID, NavigationCycleID = NavigationCycleID });
                //return View(EncounterVM);
            }
            else
            {
                var allErrors = ModelState.Values.SelectMany(v => v.Errors);
                EncounterViewModel EncounterVM1 = new EncounterViewModel();

                return View(EncounterVM1);
            }


        }

        // GET: Encounters/Edit/5
        public ActionResult Edit(int? id)
        {
            //id = 5010;

            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tEncounter tEncounter = GetEncounter((int)id);
            if (tEncounter == null)
            {
                return HttpNotFound();
            }

            if (tEncounter.NextAppointmentDt == DateTime.MinValue)
            {
                tEncounter.NextAppointmentDt = null;
            }

            if (tEncounter.ServicesTerminatedDt == DateTime.MinValue)
            {
                tEncounter.ServicesTerminatedDt = null;
            }



            EncounterViewModel EncounterVM = new EncounterViewModel();
            EncounterVM.tEncounter = tEncounter;

            List<trBarrierType> barrierTypesLookup = new List<trBarrierType>();
            List<trBarrier> barriersLookup = new List<trBarrier>();

            try
            {
                List<tBarrier> barriersList = GetBarriersFromDataSet(EncounterVM.tEncounter.EncounterID);
                EncounterVM.BarrierList = barriersList;

                barrierTypesLookup = GetBarrierTypeFromDataSet();
                EncounterVM.BarrierTypes = barrierTypesLookup;

                barriersLookup = GetBarrierFromDataSet();
                EncounterVM.Barriers = barriersLookup;

            }
            catch (Exception e)
            {
                throw; //Errors.PublishException(e, GetAppName(false), GetVersion());
            }

            return View(EncounterVM);
        }

        // POST: Encounters/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(EncounterViewModel EncounterVM, int NSID = 0, int NavigatorID = 0)//[Bind(Include = "EncounterID,FK_NavigatorID,FK_RecipientID,FK_NavigationCycleID,EncounterNumber,EncounterDt,EncounterStartTime,EncounterEndTime,EncounterType,MissedAppointment,SvcOutcomeStatus,SvcOutcomeStatusReason,NextAppointmentDt,NextAppointmentTime,NextAppointmentType,ServicesTerminated,ServicesTerminatedDt,ServicesTerminatedReason,Notes,xtag,DateCreated,CreatedBy,LastUpdated,UpdatedBy")] tEncounter tEncounter)
        {
            var BarrierID = 0;

            tBarrier tBarrier = new tBarrier();
            tSolution tSolution = new tSolution();

            if (EncounterVM.SelectedSolution == null)
            {
                TempData["ErrorMessage"] = "A Solution for the Barrier needs to be selected in order to continue. Please try again by selecting a Solution to the Barrier.";
                return RedirectToAction("Edit", "Encounters", new { NSID = NSID, NavigatorID = NavigatorID });
            }


            if (ModelState.IsValid)
            {
                if (NSID > 0) { EncounterVM.tEncounter.FK_RecipientID = NSID; }

                if (NavigatorID > 0) { EncounterVM.tEncounter.FK_NavigatorID = NavigatorID; }

                //if (NavigationCycleID > 0) { EncounterVM.tEncounter.FK_NavigationCycleID = NavigationCycleID; }

                if (EncounterVM.tEncounter.NextAppointmentDt == null)
                {
                    EncounterVM.tEncounter.NextAppointmentDt = DateTime.MinValue;
                }

                if (EncounterVM.tEncounter.ServicesTerminatedDt == null)
                {
                    EncounterVM.tEncounter.ServicesTerminatedDt = DateTime.MinValue;
                }

                try
                {
                    getProxy().UpdateEncounter(EncounterVM.tEncounter.EncounterID, EncounterVM.tEncounter.FK_NavigatorID, EncounterVM.tEncounter.FK_RecipientID,
                    EncounterVM.tEncounter.EncounterNumber, EncounterVM.tEncounter.EncounterDt, EncounterVM.tEncounter.EncounterStartTime, EncounterVM.tEncounter.EncounterEndTime, EncounterVM.tEncounter.EncounterType,
                    EncounterVM.tEncounter.EncounterReason, EncounterVM.tEncounter.MissedAppointment, EncounterVM.tEncounter.SvcOutcomeStatus, EncounterVM.tEncounter.SvcOutcomeStatusReason, EncounterVM.tEncounter.NextAppointmentDt,
                    EncounterVM.tEncounter.NextAppointmentTime, EncounterVM.tEncounter.NextAppointmentType, EncounterVM.tEncounter.ServicesTerminated, EncounterVM.tEncounter.ServicesTerminatedDt, EncounterVM.tEncounter.ServicesTerminatedReason,
                    EncounterVM.tEncounter.Notes);

                    //Check whether a new Barrier was added by user and then insert into DB

                    if (EncounterVM.SelectedBarrierType > 0)
                    {
                        //Create the tBarrier object
                        tBarrier.BarrierType = EncounterVM.SelectedBarrierType;
                        tBarrier.BarrierAssessed = EncounterVM.SelectedBarrier;
                        tBarrier.CreatedBy = "";
                        tBarrier.FK_EncounterID = EncounterVM.tEncounter.EncounterID;
                        tBarrier.FollowUpDt = DateTime.MinValue;


                        //Insert Barrier and get Barrier ID
                        BarrierID = getProxy().InsertBarrier(EncounterVM.tEncounter.EncounterID, tBarrier.BarrierType, tBarrier.BarrierAssessed, tBarrier.FollowUpDt);

                        string Solutions = EncounterVM.SelectedSolution;
                        string[] solutions = Solutions.Split(',');

                        string FollowupDt = EncounterVM.SelectedFollowupDt;
                        string[] followupdt = FollowupDt.Split(',');

                        string ImplementationStatus = EncounterVM.SelectedImplementationStatus;
                        string[] implementationstatus = ImplementationStatus.Split(',');


                        for (int i = 0; i < solutions.Count(); i++)
                        {
                            string sol = solutions[i].Substring(solutions[i].IndexOf(":") + 1);
                            string fup = followupdt[i].Substring(followupdt[i].IndexOf(":") + 1);
                            string imp = implementationstatus[i].Substring(implementationstatus[i].IndexOf(":") + 1);

                            if (sol != "") //Solution is checked
                            {

                                tSolution.SolutionOffered = Convert.ToInt32(sol);
                                if (fup != "")
                                { tSolution.FollowUpDt = Convert.ToDateTime(fup); }
                                else
                                { tSolution.FollowUpDt = DateTime.MinValue; }

                                tSolution.SolutionImplementationStatus = Convert.ToInt32(imp);

                                getProxy().InsertSolution(BarrierID, tSolution.SolutionOffered, tSolution.SolutionImplementationStatus, tSolution.FollowUpDt);
                            }
                        }

                    }

                    try
                    {
                        List<tBarrier> barriersList = GetBarriersFromDataSet(EncounterVM.tEncounter.EncounterID);
                        EncounterVM.BarrierList = barriersList;

                    }
                    catch (Exception e)
                    {
                        throw; //Errors.PublishException(e, GetAppName(false), GetVersion());
                    }

                }
                catch (System.Data.Entity.Validation.DbEntityValidationException e)
                {
                    foreach (var eve in e.EntityValidationErrors)
                    {
                        Console.WriteLine("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                            eve.Entry.Entity.GetType().Name, eve.Entry.State);
                        foreach (var ve in eve.ValidationErrors)
                        {
                            Console.WriteLine("- Property: \"{0}\", Error: \"{1}\"",
                                ve.PropertyName, ve.ErrorMessage);
                        }
                    }
                    throw; //Errors.PublishException(e, GetAppName(false), GetVersion());
                }
                //return View(EncounterVM);

                TempData["FeedbackMessage"] = "Your changes were saved successfully.";

                return RedirectToAction("Edit", "Encounters", new { NSID = EncounterVM.tEncounter.FK_RecipientID, NavigatorID = EncounterVM.tEncounter.FK_NavigatorID });
            }
            else
            {
                var allErrors = ModelState.Values.SelectMany(v => v.Errors);

                tEncounter tEncounter = GetEncounter(EncounterVM.tEncounter.EncounterID);

                EncounterViewModel encounterVM = new EncounterViewModel();
                EncounterVM.tEncounter = tEncounter;

                try
                {
                    List<tBarrier> barriersList = GetBarriersFromDataSet(encounterVM.tEncounter.EncounterID);
                    encounterVM.BarrierList = barriersList;

                }
                catch (Exception e)
                {
                    throw; //Errors.PublishException(e, GetAppName(false), GetVersion());
                }

                return View(encounterVM);
            }

        }

        public ActionResult BarrierEntryRow()
        {
            EncounterViewModel EncounterVM = new EncounterViewModel();
            return PartialView("_Barrier", EncounterVM);
        }

        [HttpPost]
        [OutputCache(Location = System.Web.UI.OutputCacheLocation.None)]
        public ActionResult BarrierTypePost(int paramBarrierType)
        {

            List<trBarrier> objbarrier = new List<trBarrier>();
            objbarrier = GetBarrierLookup().Where(m => m.BarrierTypeCode == paramBarrierType).ToList();
            SelectList obgbarrier = new SelectList(objbarrier, "BarrierCode", "BarrierDescription", 0);
            return Json(obgbarrier);

        }

        [HttpPost]
        [OutputCache(Location = System.Web.UI.OutputCacheLocation.None)]
        public ActionResult BarrierPost(int paramBarrierType)
        {

            List<trBarrierSolution> objbarriersolution = new List<trBarrierSolution>();
            objbarriersolution = GetBarrierSolutionLookup().Where(m => m.BarrierTypeCode == paramBarrierType).ToList();
            SelectList obgbarriersolution = new SelectList(objbarriersolution, "BarrierSolutionCode", "BarrierSolutionDescription", 0);
            return Json(obgbarriersolution);

        }




        #region Get Encounter
        private tEncounter GetEncounter(int EncounterID)
        {
            tEncounter tEncounter = new tEncounter();
            try
            {
                DataSet encounterDS = getProxy().GetEncounter(EncounterID);
                for (var index = 0; index < encounterDS.Tables[0].Rows.Count; index++)
                {

                    tEncounter = new tEncounter()
                    {
                        EncounterID = (int)encounterDS.Tables[0].Rows[index]["EncounterID"],
                        FK_NavigatorID = (int)encounterDS.Tables[0].Rows[index]["FK_NavigatorID"],
                        FK_RecipientID = (int)encounterDS.Tables[0].Rows[index]["FK_RecipientID"],
                        //FK_NavigationCycleID = (int)encounterDS.Tables[0].Rows[index]["FK_NavigationCycleID"],
                        EncounterNumber = (int)encounterDS.Tables[0].Rows[index]["EncounterNumber"],
                        EncounterDt = (DateTime)encounterDS.Tables[0].Rows[index]["EncounterDt"],
                        EncounterStartTime = encounterDS.Tables[0].Rows[index]["EncounterStartTime"].ToString(),
                        EncounterEndTime = encounterDS.Tables[0].Rows[index]["EncounterEndTime"].ToString(),
                        EncounterType = (int)encounterDS.Tables[0].Rows[index]["EncounterType"],
                        EncounterReason = (int)encounterDS.Tables[0].Rows[index]["EncounterReason"],
                        MissedAppointment = Convert.IsDBNull(encounterDS.Tables[0].Rows[index]["MissedAppointment"]) ? false : (bool)encounterDS.Tables[0].Rows[index]["MissedAppointment"],
                        SvcOutcomeStatus = Convert.IsDBNull(encounterDS.Tables[0].Rows[index]["SvcOutcomeStatus"]) ? 0 : (int)encounterDS.Tables[0].Rows[index]["SvcOutcomeStatus"],
                        SvcOutcomeStatusReason = encounterDS.Tables[0].Rows[index]["SvcOutcomeStatusReason"].ToString(),
                        NextAppointmentDt = Convert.IsDBNull(encounterDS.Tables[0].Rows[index]["NextAppointmentDt"]) ? DateTime.MinValue : (DateTime)encounterDS.Tables[0].Rows[index]["NextAppointmentDt"],
                        NextAppointmentTime = encounterDS.Tables[0].Rows[index]["NextAppointmentTime"].ToString(),
                        NextAppointmentType = Convert.IsDBNull(encounterDS.Tables[0].Rows[index]["NextAppointmentType"]) ? 0 : (int?)encounterDS.Tables[0].Rows[index]["NextAppointmentType"],
                        ServicesTerminated = Convert.IsDBNull(encounterDS.Tables[0].Rows[index]["ServicesTerminated"]) ? false : (bool)encounterDS.Tables[0].Rows[index]["ServicesTerminated"],
                        ServicesTerminatedDt = Convert.IsDBNull(encounterDS.Tables[0].Rows[index]["ServicesTerminatedDt"]) ? DateTime.MinValue : (DateTime)encounterDS.Tables[0].Rows[index]["ServicesTerminatedDt"],
                        ServicesTerminatedReason = encounterDS.Tables[0].Rows[index]["ServicesTerminatedReason"].ToString(),
                        Notes = encounterDS.Tables[0].Rows[index]["Notes"].ToString(),
                        xtag = encounterDS.Tables[0].Rows[index]["xtag"].ToString(),
                        DateCreated = Convert.IsDBNull(encounterDS.Tables[0].Rows[index]["DateCreated"]) ? DateTime.MinValue : (DateTime)encounterDS.Tables[0].Rows[index]["DateCreated"],
                        CreatedBy = encounterDS.Tables[0].Rows[index]["CreatedBy"].ToString(),
                        LastUpdated = Convert.IsDBNull(encounterDS.Tables[0].Rows[index]["LastUpdated"]) ? DateTime.MinValue : (DateTime)encounterDS.Tables[0].Rows[index]["LastUpdated"],
                        UpdatedBy = encounterDS.Tables[0].Rows[index]["UpdatedBy"].ToString()
                    };
                }

            }
            catch
            {
                throw;
            }

            return tEncounter;
        }

        #endregion

        #region Get Encounter List
        private List<tEncounter> GetEncounterList(int NSID)
        {
            List<tEncounter> lsEncounters = new List<tEncounter>();

            try
            {
                DataSet encounterDS = getProxy().SelectEncounter(NSID);
                for (var index = 0; index < encounterDS.Tables[0].Rows.Count; index++)
                {

                    lsEncounters.Add(new tEncounter()
                    {
                        EncounterID = (int)encounterDS.Tables[0].Rows[index]["EncounterID"],
                        FK_NavigatorID = (int)encounterDS.Tables[0].Rows[index]["FK_NavigatorID"],
                        FK_RecipientID = (int)encounterDS.Tables[0].Rows[index]["FK_RecipientID"],
                        //FK_NavigationCycleID = (int)encounterDS.Tables[0].Rows[index]["FK_NavigationCycleID"],
                        EncounterNumber = (int)encounterDS.Tables[0].Rows[index]["EncounterNumber"],
                        EncounterDt = (DateTime)encounterDS.Tables[0].Rows[index]["EncounterDt"],
                        EncounterStartTime = encounterDS.Tables[0].Rows[index]["EncounterStartTime"].ToString(),
                        EncounterEndTime = encounterDS.Tables[0].Rows[index]["EncounterEndTime"].ToString(),
                        EncounterType = (int)encounterDS.Tables[0].Rows[index]["EncounterType"],
                        EncounterReason = (int)encounterDS.Tables[0].Rows[index]["EncounterReason"],
                        MissedAppointment = Convert.IsDBNull(encounterDS.Tables[0].Rows[index]["MissedAppointment"]) ? false : (bool)encounterDS.Tables[0].Rows[index]["MissedAppointment"],
                        SvcOutcomeStatus = Convert.IsDBNull(encounterDS.Tables[0].Rows[index]["SvcOutcomeStatus"]) ? 0 : (int)encounterDS.Tables[0].Rows[index]["SvcOutcomeStatus"],
                        SvcOutcomeStatusReason = encounterDS.Tables[0].Rows[index]["SvcOutcomeStatusReason"].ToString(),
                        NextAppointmentDt = Convert.IsDBNull(encounterDS.Tables[0].Rows[index]["NextAppointmentDt"]) ? DateTime.MinValue : (DateTime)encounterDS.Tables[0].Rows[index]["NextAppointmentDt"],
                        NextAppointmentTime = encounterDS.Tables[0].Rows[index]["NextAppointmentTime"].ToString(),
                        NextAppointmentType = Convert.IsDBNull(encounterDS.Tables[0].Rows[index]["NextAppointmentType"]) ? 0 : (int?)encounterDS.Tables[0].Rows[index]["NextAppointmentType"],
                        ServicesTerminated = Convert.IsDBNull(encounterDS.Tables[0].Rows[index]["ServicesTerminated"]) ? false : (bool)encounterDS.Tables[0].Rows[index]["ServicesTerminated"],
                        ServicesTerminatedDt = Convert.IsDBNull(encounterDS.Tables[0].Rows[index]["ServicesTerminatedDt"]) ? DateTime.MinValue : (DateTime)encounterDS.Tables[0].Rows[index]["ServicesTerminatedDt"],
                        ServicesTerminatedReason = encounterDS.Tables[0].Rows[index]["ServicesTerminatedReason"].ToString(),
                        Notes = encounterDS.Tables[0].Rows[index]["Notes"].ToString(),
                        xtag = encounterDS.Tables[0].Rows[index]["xtag"].ToString(),
                        DateCreated = Convert.IsDBNull(encounterDS.Tables[0].Rows[index]["DateCreated"]) ? DateTime.MinValue : (DateTime)encounterDS.Tables[0].Rows[index]["DateCreated"],
                        CreatedBy = encounterDS.Tables[0].Rows[index]["CreatedBy"].ToString(),
                        LastUpdated = Convert.IsDBNull(encounterDS.Tables[0].Rows[index]["LastUpdated"]) ? DateTime.MinValue : (DateTime)encounterDS.Tables[0].Rows[index]["LastUpdated"],
                        UpdatedBy = encounterDS.Tables[0].Rows[index]["UpdatedBy"].ToString()
                    });
                }

            }
            catch
            {
                throw;
            }

            return lsEncounters;
        }

        #endregion

        #region Get Barriers From DataSet
        private List<tBarrier> GetBarriersFromDataSet(int EncounterID)
        {
            List<tBarrier> lsBarriers = new List<tBarrier>();
            try
            {
                DataSet barriersDS = getProxy().GetBarriersList(EncounterID);
                for (var index = 0; index < barriersDS.Tables[0].Rows.Count; index++)
                {

                    lsBarriers.Add(new tBarrier()
                    {
                        BarrierID = (int)barriersDS.Tables[0].Rows[index]["BarrierID"],
                        FK_EncounterID = (int)barriersDS.Tables[0].Rows[index]["FK_EncounterID"],
                        BarrierType = (int)barriersDS.Tables[0].Rows[index]["BarrierType"],
                        BarrierAssessed = (int)barriersDS.Tables[0].Rows[index]["BarrierAssessed"],
                        FollowUpDt = Convert.IsDBNull(barriersDS.Tables[0].Rows[index]["FollowUpDt"]) ? DateTime.MinValue : (DateTime)barriersDS.Tables[0].Rows[index]["FollowUpDt"],
                        xtag = barriersDS.Tables[0].Rows[index]["xtag"].ToString(),
                        DateCreated = Convert.IsDBNull(barriersDS.Tables[0].Rows[index]["DateCreated"]) ? DateTime.MinValue : (DateTime)barriersDS.Tables[0].Rows[index]["DateCreated"],
                        CreatedBy = barriersDS.Tables[0].Rows[index]["CreatedBy"].ToString(),
                        LastUpdated = Convert.IsDBNull(barriersDS.Tables[0].Rows[index]["LastUpdated"]) ? DateTime.MinValue : (DateTime)barriersDS.Tables[0].Rows[index]["LastUpdated"],
                        UpdatedBy = barriersDS.Tables[0].Rows[index]["UpdatedBy"].ToString()
                    });
                }

            }
            catch
            {
                throw;
            }

            return lsBarriers;
        }

        #endregion

        #region Get Solutions From DataSet
        private List<tSolution> GetSolutionsFromDataSet(int BarrierID)
        {
            List<tSolution> lsSolutions = new List<tSolution>();
            try
            {
                DataSet solutionsDS = getProxy().GetSolutionsList(BarrierID);
                for (var index = 0; index < solutionsDS.Tables[0].Rows.Count; index++)
                {

                    lsSolutions.Add(new tSolution()
                    {
                        SolutionID = (int)solutionsDS.Tables[0].Rows[index]["SolutionID"],
                        FK_BarrierID = (int)solutionsDS.Tables[0].Rows[index]["FK_BarrierID"],
                        SolutionOffered = (int)solutionsDS.Tables[0].Rows[index]["SolutionOffered"],
                        SolutionImplementationStatus = Convert.IsDBNull(solutionsDS.Tables[0].Rows[index]["SolutionImplementationStatus"]) ? 0 : (int)solutionsDS.Tables[0].Rows[index]["SolutionImplementationStatus"]
                    });

                }

            }
            catch
            {
                throw;
            }

            return lsSolutions;
        }

        #endregion

        #region Get Barrier
        private tBarrier GetBarrier(int BarrierID)
        {
            tBarrier tbarrier = new tBarrier();
            try
            {
                DataSet barriersDS = getProxy().GetBarrier(BarrierID);
                for (var index = 0; index < barriersDS.Tables[0].Rows.Count; index++)
                {

                    tbarrier = new tBarrier()
                    {
                        BarrierID = (int)barriersDS.Tables[0].Rows[index]["BarrierID"],
                        FK_EncounterID = (int)barriersDS.Tables[0].Rows[index]["FK_EncounterID"],
                        BarrierType = (int)barriersDS.Tables[0].Rows[index]["BarrierType"],
                        BarrierAssessed = (int)barriersDS.Tables[0].Rows[index]["BarrierAssessed"],
                        FollowUpDt = (DateTime)barriersDS.Tables[0].Rows[index]["FollowUpDt"],
                        xtag = barriersDS.Tables[0].Rows[index]["xtag"].ToString(),
                        DateCreated = Convert.IsDBNull(barriersDS.Tables[0].Rows[index]["DateCreated"]) ? DateTime.MinValue : (DateTime)barriersDS.Tables[0].Rows[index]["DateCreated"],
                        CreatedBy = barriersDS.Tables[0].Rows[index]["CreatedBy"].ToString(),
                        LastUpdated = Convert.IsDBNull(barriersDS.Tables[0].Rows[index]["LastUpdated"]) ? DateTime.MinValue : (DateTime)barriersDS.Tables[0].Rows[index]["LastUpdated"],
                        UpdatedBy = barriersDS.Tables[0].Rows[index]["UpdatedBy"].ToString()
                    };
                }

            }
            catch
            {
                throw;
            }

            return tbarrier;
        }

        #endregion

        #region Get Barrier Type Lookup
        public static List<SelectListItem> GetBarrierTypeLookup()
        {
            List<SelectListItem> ls = new List<SelectListItem>();
            //get values from web service

            DataSet barrierTypeDS = getProxy().SelectBarrierType();

            try
            {
                for (var index = 0; index < barrierTypeDS.Tables[0].Rows.Count; index++)
                {
                    ls.Add(new SelectListItem() { Text = barrierTypeDS.Tables[0].Rows[index]["BarrierTypeDescription"].ToString(), Value = barrierTypeDS.Tables[0].Rows[index]["BarrierTypeCode"].ToString() });
                }

            }
            catch (Exception e)
            {
                throw; //Errors.PublishException(e, GetAppName(false), GetVersion());
            }


            return ls;
        }
        #endregion

        #region Get Barrier Lookup
        public static List<trBarrier> GetBarrierLookup()
        {

            List<trBarrier> ls = new List<trBarrier>();

            DataSet barrierDS = getProxy().SelectBarriers();

            try
            {
                for (var index = 0; index < barrierDS.Tables[0].Rows.Count; index++)
                {

                    ls.Add(new trBarrier()
                    {
                        BarrierDescription = barrierDS.Tables[0].Rows[index]["BarrierDescription"].ToString(),
                        BarrierCode = (int)barrierDS.Tables[0].Rows[index]["BarrierCode"],
                        BarrierTypeCode = (int)barrierDS.Tables[0].Rows[index]["BarrierTypeCode"]
                    });


                }

            }
            catch (Exception e)
            {
                throw; //Errors.PublishException(e, GetAppName(false), GetVersion());
            }

            return ls;
        }

        #endregion

        #region Get Barrier Solution Lookup
        public static List<trBarrierSolution> GetBarrierSolutionLookup()
        {
            List<trBarrierSolution> ls = new List<trBarrierSolution>();

            DataSet barrierSolutionDS = getProxy().SelectBarrierSolutions();

            try
            {
                for (var index = 0; index < barrierSolutionDS.Tables[0].Rows.Count; index++)
                {
                    ls.Add(new trBarrierSolution()
                    {
                        BarrierSolutionDescription = barrierSolutionDS.Tables[0].Rows[index]["BarrierSolutionDescription"].ToString(),
                        BarrierSolutionCode = (int)barrierSolutionDS.Tables[0].Rows[index]["BarrierSolutionCode"],
                        BarrierTypeCode = (int)barrierSolutionDS.Tables[0].Rows[index]["BarrierTypeCode"]
                    });

                }

            }
            catch (Exception e)
            {
                throw; //Errors.PublishException(e, GetAppName(false), GetVersion());
            }

            return ls;
        }

        #endregion

        #region Get Barrier Type Lookup from Dataset
        public static List<trBarrierType> GetBarrierTypeFromDataSet()
        {
            List<trBarrierType> ls = new List<trBarrierType>();
            //get values from web service

            DataSet barrierTypeDS = getProxy().SelectBarrierType();

            try
            {
                for (var index = 0; index < barrierTypeDS.Tables[0].Rows.Count; index++)
                {
                    ls.Add(new trBarrierType() { BarrierTypeDescription = barrierTypeDS.Tables[0].Rows[index]["BarrierTypeDescription"].ToString(), BarrierTypeCode = (int)barrierTypeDS.Tables[0].Rows[index]["BarrierTypeCode"] });
                }

            }
            catch (Exception e)
            {
                throw; //Errors.PublishException(e, GetAppName(false), GetVersion());
            }


            return ls;
        }
        #endregion

        #region Get Barrier Lookup from Dataset
        public static List<trBarrier> GetBarrierFromDataSet()
        {
            List<trBarrier> ls = new List<trBarrier>();
            //get values from web service

            DataSet barrierDS = getProxy().SelectBarriers();

            try
            {
                for (var index = 0; index < barrierDS.Tables[0].Rows.Count; index++)
                {
                    ls.Add(new trBarrier() { BarrierDescription = barrierDS.Tables[0].Rows[index]["BarrierDescription"].ToString(), BarrierCode = (int)barrierDS.Tables[0].Rows[index]["BarrierCode"] });
                }

            }
            catch (Exception e)
            {
                throw; //Errors.PublishException(e, GetAppName(false), GetVersion());
            }


            return ls;
        }
        #endregion

        #region Get Recipients From DataSet
        private List<tServiceRecipient> GetRecipientsFromDataSet(int NavigatorID)
        {
            List<tServiceRecipient> lsRecipients = new List<tServiceRecipient>();
            try
            {
                DataSet recipientsDS = getProxy().SelectServiceRecipient(NavigatorID);
                for (var index = 0; index < recipientsDS.Tables[0].Rows.Count; index++)
                {

                    lsRecipients.Add(new tServiceRecipient()
                    {
                        NSRecipientID = (int)recipientsDS.Tables[0].Rows[index]["NSRecipientID"],
                        FK_NavigatorID = (int)recipientsDS.Tables[0].Rows[index]["FK_NavigatorID"],
                        FK_RecipID = recipientsDS.Tables[0].Rows[index]["FK_RecipID"].ToString(),
                        LastName = recipientsDS.Tables[0].Rows[index]["LastName"].ToString(),
                        FirstName = recipientsDS.Tables[0].Rows[index]["FirstName"].ToString(),
                        MiddleInitial = recipientsDS.Tables[0].Rows[index]["MiddleInitial"].ToString(),
                        DOB = (DateTime)recipientsDS.Tables[0].Rows[index]["DOB"],
                        Address1 = recipientsDS.Tables[0].Rows[index]["Address1"].ToString(),
                        Address2 = recipientsDS.Tables[0].Rows[index]["Address2"].ToString(),
                        City = recipientsDS.Tables[0].Rows[index]["City"].ToString(),
                        State = recipientsDS.Tables[0].Rows[index]["State"].ToString(),
                        Zip = recipientsDS.Tables[0].Rows[index]["Zip"].ToString(),
                        Phone1 = recipientsDS.Tables[0].Rows[index]["Phone1"].ToString(),
                        P1PhoneType = (short)recipientsDS.Tables[0].Rows[index]["P1PhoneType"],
                        P1PersonalMessage = Convert.IsDBNull(recipientsDS.Tables[0].Rows[index]["P1PersonalMessage"]) ? false : (bool)recipientsDS.Tables[0].Rows[index]["P1PersonalMessage"],
                        Phone2 = recipientsDS.Tables[0].Rows[index]["Phone2"].ToString(),
                        P2PhoneType = Convert.IsDBNull(recipientsDS.Tables[0].Rows[index]["P2PhoneType"]) ? 0 : (short?)recipientsDS.Tables[0].Rows[index]["P2PhoneType"],
                        P2PersonalMessage = Convert.IsDBNull(recipientsDS.Tables[0].Rows[index]["P2PersonalMessage"]) ? false : (bool)recipientsDS.Tables[0].Rows[index]["P2PersonalMessage"],
                        MotherMaidenName = recipientsDS.Tables[0].Rows[index]["MotherMaidenName"].ToString(),
                        Email = recipientsDS.Tables[0].Rows[index]["Email"].ToString(),
                        SSN = recipientsDS.Tables[0].Rows[index]["SSN"].ToString(),
                        ImmigrantStatus = Convert.IsDBNull(recipientsDS.Tables[0].Rows[index]["ImmigrantStatus"]) ? false : (bool)recipientsDS.Tables[0].Rows[index]["ImmigrantStatus"],
                        CountryOfOrigin = Convert.IsDBNull(recipientsDS.Tables[0].Rows[index]["CountryOfOrigin"]) ? 0 : (short?)recipientsDS.Tables[0].Rows[index]["CountryOfOrigin"],
                        Gender = recipientsDS.Tables[0].Rows[index]["Gender"].ToString(),
                        Ethnicity = Convert.IsDBNull(recipientsDS.Tables[0].Rows[index]["Ethnicity"]) ? false : (bool)recipientsDS.Tables[0].Rows[index]["Ethnicity"],
                        PrimaryLanguage = (short)recipientsDS.Tables[0].Rows[index]["PrimaryLanguage"],
                        AgreeToSpeakEnglish = Convert.IsDBNull(recipientsDS.Tables[0].Rows[index]["AgreeToSpeakEnglish"]) ? false : (bool)recipientsDS.Tables[0].Rows[index]["AgreeToSpeakEnglish"],
                        CaregiverName = recipientsDS.Tables[0].Rows[index]["CaregiverName"].ToString(),
                        Relationship = Convert.IsDBNull(recipientsDS.Tables[0].Rows[index]["Relationship"]) ? 0 : (short?)recipientsDS.Tables[0].Rows[index]["Relationship"],
                        RelationshipOther = recipientsDS.Tables[0].Rows[index]["RelationshipOther"].ToString(),
                        CaregiverPhone = recipientsDS.Tables[0].Rows[index]["CaregiverPhone"].ToString(),
                        CaregiverPhoneType = Convert.IsDBNull(recipientsDS.Tables[0].Rows[index]["CaregiverPhoneType"]) ? 0 : (short?)recipientsDS.Tables[0].Rows[index]["CaregiverPhoneType"],
                        CaregiverPhonePersonalMessage = Convert.IsDBNull(recipientsDS.Tables[0].Rows[index]["CaregiverPhonePersonalMessage"]) ? false : (bool)recipientsDS.Tables[0].Rows[index]["CaregiverPhonePersonalMessage"],
                        CaregiverEmail = recipientsDS.Tables[0].Rows[index]["CaregiverEmail"].ToString(),
                        CaregiverContactPreference = Convert.IsDBNull(recipientsDS.Tables[0].Rows[index]["CaregiverContactPreference"]) ? 0 : (int)recipientsDS.Tables[0].Rows[index]["CaregiverContactPreference"],
                        Comments = recipientsDS.Tables[0].Rows[index]["Comments"].ToString(),
                        xtag = recipientsDS.Tables[0].Rows[index]["xtag"].ToString(),
                        DateCreated = Convert.IsDBNull(recipientsDS.Tables[0].Rows[index]["DateCreated"]) ? DateTime.MinValue : (DateTime)recipientsDS.Tables[0].Rows[index]["DateCreated"],
                        CreatedBy = recipientsDS.Tables[0].Rows[index]["CreatedBy"].ToString(),
                        LastUpdated = Convert.IsDBNull(recipientsDS.Tables[0].Rows[index]["LastUpdated"]) ? DateTime.MinValue : (DateTime)recipientsDS.Tables[0].Rows[index]["LastUpdated"],
                        UpdatedBy = recipientsDS.Tables[0].Rows[index]["UpdatedBy"].ToString()
                    });
                }

            }
            catch
            {
                throw;
            }

            return lsRecipients;
        }

        #endregion

        #region GetSvcOutcomeStatusDropDown
        public static List<SelectListItem> GetSvcOutcomeStatusDropDown()
        {
            List<SelectListItem> ls = new List<SelectListItem>();

            ls.Add(new SelectListItem() { Text = "Not Met", Value = "1" });
            ls.Add(new SelectListItem() { Text = "Met", Value = "2" });

            return ls;
        }

        #endregion

    }
}
