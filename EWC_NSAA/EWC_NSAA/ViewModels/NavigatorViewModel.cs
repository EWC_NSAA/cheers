﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using EWC_NSAA.Models;

namespace EWC_NSAA.ViewModels
{
    public class NavigatorViewModel
    {
        public tNavigator tNavigator { get; set; }
                
        public int NavigatorID { get; set; }
               
        public int Region { get; set; }

        [Required]
        public int Type { get; set; }

        [Required]
        [RegularExpression("^(\\w+[,.]?[ ]?|\\w+['-]?)+$", ErrorMessage ="Invalid Last Name")]
        public string LastName { get; set; }

        [Required]
        [RegularExpression("^(\\w+[,.]?[ ]?|\\w+['-]?)+$", ErrorMessage = "Invalid First Name")]
        public string FirstName { get; set; }

        [Required]
        [RegularExpression("\\d+[ ](?:[A-Za-z0-9.-]+[ ]?)+(?:Avenue|Lane|Road|Boulevard|Drive|Street|Ave|Dr|Rd|Blvd|Ln|St)\\.?", ErrorMessage = "Invalid Address")]
        public string Address1 { get; set; }
        public string Address2 { get; set; }

        [Required]
        [RegularExpression("^[a-zA-Z-\\s]+$", ErrorMessage = "Invalid City")]
        public string City { get; set; }

        [Required]
        [StringLength(2)]
        [RegularExpression("^[a-zA-Z-\\s]+$", ErrorMessage = "Invalid State")]
        public string State { get; set; }

        [Required]        
        [RegularExpression("^\\d{5}(?:[-\\s]\\d{4})?$", ErrorMessage = "Invalid Zip Code")]
        public string Zip { get; set; }

        [Required]
        [EmailAddress(ErrorMessage = "Invalid Email Address")]
        public string Email { get; set; }
        public string DomainName { get; set; }

        [Required]
        [DataType(DataType.PhoneNumber, ErrorMessage = "Provided phone number not valid")]
        public string BusinessTelephone { get; set; }
        public string MobileTelephone { get; set; }

        public IEnumerable<NavigatorTraining> Trainings { get; set; }

        public int TrainingID { get; set; }
        public int FK_NavigatorID { get; set; }
        public string CourseName { get; set; }
        public string Organization { get; set; }
        public DateTime CompletionDt { get; set; }
        public bool Certificate { get; set; }

        public IEnumerable<NavigatorLanguages> Languages { get; set; }

        public int NavigatorLanguageID { get; set; }
        public int LanguageCode { get; set; }
        public int SpeakingScore { get; set; }
        public int ListeningScore { get; set; }
        public int ReadingScore { get; set; }
        public int WritingScore { get; set; }
        public string LanguageName { get; set; }
        public IEnumerable<trProvLanguages> RLanguages { get; set; }

    }
}