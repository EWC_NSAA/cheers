﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using EWC_NSAA.Models;
using EWC_NSAA.App_Start;
using EWC_NSAA.Common;
using EWC_NSAA.Controllers;

namespace EWC_NSAA
{
    
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()

        {
            AreaRegistration.RegisterAllAreas();
            //FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);

            MvcHandler.DisableMvcResponseHeader = true;
        }

        void Session_Start(object sender, EventArgs e)
        {
            AuditTrail AA = new AuditTrail();
            AA.Init();

        }

        protected void Application_Error(object sender, EventArgs e)
        {
            
            HttpContext ctx = HttpContext.Current;
            Exception ex = ctx.Server.GetLastError();
            ctx.Server.ClearError();

            if (ex != null)
            {
                Errors.PublishException(ex, BaseController.GetAppName(false), BaseController.GetVersion());
                Response.Redirect("~/Error.html");
            }

        }
   
    }
}
