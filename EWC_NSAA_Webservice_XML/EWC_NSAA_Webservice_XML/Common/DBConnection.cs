﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Common;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using System.Configuration;
using System.Web.Configuration;
using System.Web.Services.Protocols;

/// <summary>
/// Summary description for DBConnection
/// </summary>
public class DBConnection : BaseClass
{
    #region "Get Connection String"
        /* -------------------------------------------------------------------------
        '   Function:       GetConnectionString
        '
        '   Description:    To get the Connection String from web.config
        '        					
        '   Parameters:     None
        '
        '   Return Value:   String
        '
        '   
        '   Side Effects:	
        '
        '	Modifications:
        '		By						Date			Purpose
        '		-----------------		--------		-------------------------------
        '		Bilwa Buchake			01/07/2009		created
        '
        ' ------------------------------------------------------------------------- */
        public string GetConnectionString()
        {

        //Retrieve ConnectionString from the web.config ConnectionStrings section
        string strConnectionString = "";
            //Dim oConfig As Configuration

            try
            {
                strConnectionString = WebConfigurationManager.ConnectionStrings["EWC_NSAA_ConnStr"].ToString();
            }   
            catch (SoapException ex)
            {
            throw RaiseException("DBConnection.vb", "DMRSService", ex.Message, ErrorCode.ConnectionStringMissing.ToString(), ex.Source, (int)FaultCode.Server);
            }

            return strConnectionString;

    }
            
    #endregion

    #region "Get Database"
        /* -------------------------------------------------------------------------
        '   Function:       GetDatabase
        '
        '   Description:    To get a SqlDatabase object for performing all Data related functions
        '        					
        '   Parameters:     None
        '
        '   Return Value:   Object of Type SqlDatabase
        '
        '   
        '   Side Effects:	
        '
        '	Modifications:
        '		By						Date			Purpose
        '		-----------------		--------		-------------------------------
        '		Bilwa Buchake			01/07/2009		created
        '
        ' ------------------------------------------------------------------------- */
        public SqlDatabase GetDatabase()
        {
        //Get the Connection String for connecting to the database
        string ConnStr = GetConnectionString();

        try
        {
            SqlDatabase db = new SqlDatabase(ConnStr);

            return db;
        }
            
        catch (SqlException ex)
        {
            throw RaiseException("DBConnection.vb", "DMRSService", ex.Message, ErrorCode.DatabaseConnectionFailed.ToString(), ex.Source, (int)FaultCode.Server);
        }

     }


    #endregion

}