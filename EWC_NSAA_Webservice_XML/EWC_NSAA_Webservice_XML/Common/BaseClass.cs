﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Web;
using System.Web.Services.Protocols;
using System.Xml;
using System.Net.Mail;

/// <summary>
/// Summary description for BaseClass
/// </summary>
public class BaseClass
{

    #region "Enum FaultCode"

    public enum FaultCode
    {
        Client = 0,
        Server = 1
    }

    #endregion

    #region "Enum ErrorCode"

    public enum ErrorCode
    {
        ConnectionStringMissing = 1000,
        DatabaseConnectionFailed = 2000,
        StoredProcedureExecutionFailed = 3000,
        UnknownException = 4000,
        SecurityException = 5000
    }
        
    #endregion

    #region "Error Handling"

    /* -------------------------------------------------------------------------
    '   Function:       RaiseException
    '
    '   Description:    To return a SoapException object with information related to any exception raised
    '                   in the Web Service
    '        					
    '   Parameters:     Parameter Name          Type        Description
    '                   uri                     String      The class/Service in which the exception was raised
    '                   webServiceNamespace     String      NameSpace of the class/service where the exception was raised
    '                   errorMessage            String      Error Message for the exception
    '                   errorSource             String      Error source of the exception
    '                   code                    Integer     A code specifying whether the exception occurred at client/server
    '
    '   Return Value:   Boolean
    '
    '   
    '   Side Effects:	
    '
    '	Modifications:
    '		By						Date			Purpose
    '		-----------------		--------		-------------------------------
    '		Bilwa Buchake			04/03/2007		created
    '
    ' ------------------------------------------------------------------------- */
    public SoapException RaiseException(string uri, string webServiceNamespace, string errorMessage,
                                                string errorNumber, string errorSource, int code)

    {

        XmlQualifiedName faultCodeLocation = null;
        //Identify the location of the FaultCode
        switch(code)
        {
            case (int)FaultCode.Client:
                faultCodeLocation = SoapException.ClientFaultCode;
                break;
            case (int)FaultCode.Server:
                faultCodeLocation = SoapException.ServerFaultCode;
                break;
        }

        XmlDocument xmlDoc = new XmlDocument();
        //Create the Detail node
        XmlNode rootNode = xmlDoc.CreateNode(XmlNodeType.Element, SoapException.DetailElementName.Name,
                           SoapException.DetailElementName.Namespace);
        //Build specific details for the SoapException
        //Add first child of detail XML element.
        XmlNode errorNode = xmlDoc.CreateNode(XmlNodeType.Element, "Error", webServiceNamespace);
        //Create and set the value for the ErrorNumber node
        XmlNode errorNumberNode = xmlDoc.CreateNode(XmlNodeType.Element, "ErrorNumber", webServiceNamespace);
        errorNumberNode.InnerText = errorNumber;
        //Create and set the value for the ErrorMessage node
        XmlNode errorMessageNode = xmlDoc.CreateNode(XmlNodeType.Element, "ErrorMessage", webServiceNamespace);
        errorMessageNode.InnerText = errorMessage;
        //Create and set the value for the ErrorSource node
        XmlNode errorSourceNode = xmlDoc.CreateNode(XmlNodeType.Element, "ErrorSource", webServiceNamespace);
        errorSourceNode.InnerText = errorSource;
        //Append the Error child element nodes to the root detail node.
        errorNode.AppendChild(errorNumberNode);
        errorNode.AppendChild(errorMessageNode);
        errorNode.AppendChild(errorSourceNode);
        //Append the Detail node to the root node
        rootNode.AppendChild(errorNode);
        //Construct the exception
        SoapException soapEx = new SoapException(errorMessage, faultCodeLocation, uri, rootNode);
        //Raise the exception  back to the caller
        return soapEx;
    
    }

    #endregion

    #region "GetConfigData"

    /* -------------------------------------------------------------------------
    '   Function:       GetConfigData
    '
    '   Description:    To return data from the web.config file
    '
    '					
    '   Parameters:     msgKey		- key of message to return
    '
    '   Return Value:   the requested data as a string
    '
    '   Notes:			
    '
    '   Side Effects:	
    '
    '	Modifications:
    '		By						Date			Purpose
    '		-----------------		--------		-------------------------------
    '		robert calmann			02/02/2004		created
    '
    ' ------------------------------------------------------------------------- */
    public string GetConfigData(string msgKey)
    {
        return ConfigurationManager.AppSettings[msgKey] + "";
    }

    #endregion

    
}